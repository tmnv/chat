# chat

Клиент-серверное приложение для коммуникации.

Клонирование
- git clone git@gitlab.com:tmnv/chat.git
- git checkout -b familiya

Рабочий процесс
- git checkout master - переход в ветку мастер
- git pull origin master - скачиваете последние изменения ветки мастер из репозитория GitLab
- git checkout familiya - возвращаетесь в свою ветку
- git merge master - объединяете код своей ветки с обновлениями из ветки мастер
- РАБОТАЕТЕ В СВОЕЙ ВЕТКЕ (папка lab)
- git status - посмотреть список изменённых файлов
- git commit -a -m 'Комментарий к изменениям'
- git push - заливаете изменения своей локальной ветки в свою ветку на gitlab
- Идёте в GitLab и делаете там Merge Request
- Я вношу ваши изменения в ветку master в GitLab


# Перекинул из Quasar

# Tehnokom Chat (qchat)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
