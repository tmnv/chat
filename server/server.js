const https = require('https')
const fs = require('fs')
const path = require('path')
const WebSocket = require('ws')

const PROJECT_DIR = '/var/www/su/tehnokom'
const TLS_DIR = PROJECT_DIR + '/tls'

const HOST = 'learn.tehnokom.su'
const HTTPS_PORT = 3000

const options = {
    "key": fs.readFileSync(path.join(TLS_DIR, `${HOST}_private.key`)),
    "cert": fs.readFileSync(path.join(TLS_DIR, `${HOST}_public.crt`))
}

const httpsServer = https.createServer(options, (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html; charset=utf-8"
    })
    res.end(`<h1>WSS</h1>`)
})

httpsServer.listen(HTTPS_PORT, () => {
    console.log(`HTTPS-сервер слушает порт ${HTTPS_PORT}.`)
})

const wssServer = new WebSocket.Server({
    "server": httpsServer
})

wssServer.on('connection', ws => {
    let message = {
        'name': 'Сервер',
        'text': 'Добро пожаловать в чат сообщества Техноком.'
    }
    ws.send(addDateToMessage(message))

    ws.on('message', message => {
        wssServer.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                messageJson = JSON.parse(message)
                client.send(addDateToMessage(messageJson))
            }
        })
    })
})

const addDateToMessage = message => {
    message.date = new Date()

    return JSON.stringify(message)
}
