
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Plan.vue') },
      { path: '/soul', component: () => import('pages/Soul.vue') },
      { path: '/list', component: () => import('pages/List.vue') },
      { path: '/manifest', component: () => import('pages/Manifest.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
