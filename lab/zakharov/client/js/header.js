let header = document.getElementById('header');
let nav = document.createElement('nav');
let listLincks = [
    chat = {
        href: '/',
        text: 'Чат'
    },
    plan = {
        href: '/plan',
        text: 'План развития',
    },
    list = {
        href: '/list',
        text: 'Список участников'
    },
    universo = {
        href: 'https://universo.su/',
        text: 'Universo'
    }
]


header.appendChild(nav);
for (let i = 0; i<listLincks.length; i++) {
    let itemEl = document.createElement('a');
    
    itemEl.innerHTML = listLincks[i].text;
    itemEl.href = listLincks[i].href;
    nav.appendChild(itemEl)
};
console.log(header);
