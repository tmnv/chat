// Движки, для динамической генерации HTML-файлов
// pug          - https://pugjs.org/
// ejs          - https://ejs.co/
// Handlebars   - https://handlebarsjs.com/

const express = require('express')
const exphbs = require('express-handlebars')
const homeRoutes = require('./routes/home')
const coursesRoutes = require('./routes/courses')
const addRoutes = require('./routes/add')
const cartRoutes = require('./routes/cart')

const app = express()
// layout - макет
// Тут мы определяем основной макет страницы и расширение макета по умолчанию.
const hbs = exphbs.create({
    'defaultLayout': 'main',
    'extname': 'hbs'
})

// Тут указывается, что такой движок (шаблонизатор) вообще есть.
app.engine('hbs', hbs.engine)
// Тут указывается, что данный движок надо использовать
app.set('view engine', 'hbs')
app.set('views', `${__dirname}/views`)

// Использовать папку public для статичных файлов (стили, картинки и т.д.)
app.use(express.static(`${__dirname}/public`))
app.use(express.urlencoded({extended: true}))
app.use('/', homeRoutes)
app.use('/courses', coursesRoutes)
app.use('/add', addRoutes)
app.use('/cart', cartRoutes)

/*
Ещё надо будет добавить CSS-фраймворк для создания структуры приложения.
Используем https://materializecss.com/
Добавляется на странице head.hbs
 */

const PORT = process.env.PORT || 4000
app.listen(PORT, () => {
    console.log(`Сервер запущен на порту ${PORT}.`)
})
