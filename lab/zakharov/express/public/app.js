const toCurrency = price => {
    return new Intl.NumberFormat('ru-RU', {
        currency: 'rub',
        style: 'currency'
    }).format(price)
}

document.querySelectorAll('.price').forEach(node => {
    node.textContent = toCurrency(node.textContent)
})

const cartEl = document.querySelector('#cart')
if (cartEl) {
    cartEl.addEventListener('click', event => {
        if (event.target.classList.contains('js-remove')) {
            const id = event.target.dataset.id

            // В браузере нельзя использовать async/await для Promise, поэтому используем then.
            fetch(`/cart/remove/${id}`, {
                method: 'delete'
            }).then(res => res.json())
                .then(cart => {
                    if (cart.courses.length) {
                        const html = cart.courses.map(course => {
                            return `
                                <tr>
                                    <td>${course.title}</td>
                                    <td>${course.count}</td>
                                    <td>
                                        <button class="btn btn-small js-remove" data-id="${course.id}">Удалить</button>
                                    </td>
                                </tr>
                            `
                        }).join('')
                        cartEl.querySelector('tbody').innerHTML = html
                        cartEl.querySelector('.price').textContent = toCurrency(cart.totalPrice)
                    }
                    else {
                        cartEl.innerHTML = '<p>Корзина пуста</p>'
                    }
                })
        }
    })
}
