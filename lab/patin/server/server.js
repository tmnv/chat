const https = require('https')
const fs = require('fs')
const path = require('path')
const WebSocket = require('ws')

const PROJECT_DIR = '/var/www/su/tehnokom'
const TLS_DIR = PROJECT_DIR + '/tls'

const unit = {};

const options = {
    "key": '',
    "cert": ''
//    "key": fs.readFileSync(path.join(TLS_DIR, 'chat_private.key')),
//    "cert": fs.readFileSync(path.join(TLS_DIR, 'chat_public.crt'))
}

const port = 3001
const httpsServer = https.createServer(options, (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html; charset=utf-8"
    })
    res.end('<h1>WSS</h1>')
})

httpsServer.listen(port)

const wssServer = new WebSocket.Server({
    "server": httpsServer
    // "port": port
})

unit.outputServerPort = (portNumber) => console.log('Сервер WSS слушает порт ' + portNumber);

unit.outputServerPort(port)

wssServer.on('connection', ws => {
    let message = {
        'name': 'Сервер',
        'text': 'Добро пожаловать в чат сообщества Техноком.'
    }
    ws.send(addDateToMessage(message))

    ws.on('message', message => {
        wssServer.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                messageJson = JSON.parse(message)
                client.send(addDateToMessage(messageJson))
            }
        })
    })
})

const addDateToMessage = message => {
    message.date = new Date()

    return JSON.stringify(message)
}


module.exports = unit;