'use strict';

let expect = require('chai').expect;
let sinon = require('sinon');
let assert = sinon.assert;
let nock = require('nock');
let logStub;

const sut = require('../../server/server')

describe('When server starts it outputs data using console.log',() => {

    //Выполняется в начале describe
    before(() => {
        logStub = sinon.stub(console, 'log');
   });

    after(() => {
        logStub.restore();
    });


    beforeEach(() => {
        logStub.reset();
    });

    it('outputSeverPort with port number', () => {
    	  let portNumber = 200
        //Check that async was not called and nothing was output to log 
        assert.notCalled(logStub)
        //Output server's port
        sut.outputServerPort(portNumber)
        //Check log output
        assert.calledOnce(logStub)
        assert.calledWith(logStub, 'Сервер WSS слушает порт ' + portNumber)
    })
});
