const path = require('path')
const fs = require('fs')

const p = path.join(
    path.dirname(process.mainModule.filename),
    'data',
    'cart.json'
)

class Cart {
    static async add(addedCourse) {
        const cart = await Cart.fetch()

        const index = cart.courses.findIndex(course => course.id === addedCourse.id)
        const candidate = cart.courses[index]

        if (candidate) {
            // Курс уже есть
            candidate.count++
            cart.courses[index] = candidate
        }
        else {
            // Можно добавить
            addedCourse.count = 1
            cart.courses.push(addedCourse)
        }

        cart.totalPrice += +addedCourse.price

        return new Promise((resolve, reject) => {
            fs.writeFile(p, JSON.stringify(cart), err => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve()
                }
            })
        })
    }

    static async fetch() {
        return new Promise((resolve, reject) => {
            fs.readFile(p, 'utf-8', (err, content) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(JSON.parse(content))
                }
            })
        })
    }

    static async remove(id) {
        const cart = await Cart.fetch()

        const index = cart.courses.findIndex(course => course.id === id)
        const course = cart.courses[index]
        if (course.count === 1) {
            // Удалить
            cart.courses = cart.courses.filter(course => course.id !== id)
        }
        else {
            cart.courses[index].count--
        }

        cart.totalPrice -= course.price

        return new Promise((resolve, reject) => {
            fs.writeFile(p, JSON.stringify(cart), err => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve(cart)
                }
            })
        })
    }
}

module.exports = Cart
