// Примечание {Router} = express.Router
const {Router} = require('express')

const router = Router()

router.get('/', (req, res) => {
    // После подключения движка нам надо не отдавать страницы, а рендерить их. Поэтому вывод комментим.
    // res.status(200)
    // res.sendFile(path.join(__dirname, 'views', 'index.html'))

    // Расширение указывать не надо (но можно), так мы его указали в настройках.
    // Папку размещения страниц тоже не указываем, так как тоже указали её в настройках Express.
    res.render('index', {
        title: 'Главная страница',
        isHome: true
    })
})

module.exports = router
