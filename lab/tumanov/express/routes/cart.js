const {Router} = require('express')
const Cart = require('../models/cart')
const Course = require('../models/course')
const router = Router()

router.post('/add', async (req, res) => {
    console.log(req.body)
    const course = await Course.getById(req.body.id)
    console.log(course)
    await Cart.add(course)
    res.redirect('/cart')
})

router.get('/', async (req, res) => {
    const cart = await Cart.fetch()
    res.render('cart', {
        title: 'Корзина',
        courses: cart.courses,
        totalPrice: cart.totalPrice,
        isCart: true
    })
})

router.delete('/remove/:id', async (req, res) => {
    console.log('req.params.id', req.params.id)
    const cart = await Cart.remove(req.params.id)
    console.log(cart)
    res.status(200).json(cart)
})

module.exports = router
