class Rect {
    constructor(canvas, left, top, width, height) {
        this.canvas = canvas;

        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;

        this.draw();
    }

    moveLeft(x) {
        this.left -= x;
        this.draw();
    }

    moveRight(x) {
        this.left += x;
        this.draw();
    }

    moveUp(y) {
        this.top -= y;
        this.draw();
    }

    moveDown(y) {
        this.top += y;
        this.draw();
    }

    draw() {
        let ctx = this.canvas.getContext('2d');
        ctx.fillStyle = "white";
        ctx.fillRect(this.left, this.top, this.width, this.height);
    }
}

class Logo {
    constructor(canvas) {
        let hLineLength = 100;
        let vLineLength = 150;
        let lineThickness = 16;
        let hLineIndent = 22;
        let vLineIndent = 22;
        let initialLeftIndent = (canvas.width - (lineThickness * 2 + hLineIndent * 2 + hLineLength)) / 2;
        let initialTopIndent = (canvas.height - (lineThickness + vLineIndent + vLineLength)) / 2;

        this.canvas = canvas;
        this.leftLine = new Rect(canvas, initialLeftIndent, (initialTopIndent + lineThickness + vLineIndent), lineThickness, vLineLength);
        this.rightLine = new Rect(canvas, (canvas.width - (initialLeftIndent + lineThickness)), (initialTopIndent + lineThickness + vLineIndent), lineThickness, vLineLength);
        this.topLine = new Rect(canvas, (initialLeftIndent + lineThickness + hLineIndent), initialTopIndent, hLineLength, lineThickness);
        this.step = 25;
    }

    moveLeft() {
        this.clearCanvas();

        this.leftLine.moveLeft(this.step);
        this.rightLine.moveLeft(this.step);
        this.topLine.moveLeft(this.step);

        this.handleCollisions();
    }

    moveRight() {
        this.clearCanvas();

        this.leftLine.moveRight(this.step);
        this.rightLine.moveRight(this.step);
        this.topLine.moveRight(this.step);

        this.handleCollisions();
    }

    moveUp() {
        this.clearCanvas();

        this.leftLine.moveUp(this.step);
        this.rightLine.moveUp(this.step);
        this.topLine.moveUp(this.step);

        this.handleCollisions();
    }

    moveDown() {
        this.clearCanvas();

        this.leftLine.moveDown(this.step);
        this.rightLine.moveDown(this.step);
        this.topLine.moveDown(this.step);

        this.handleCollisions();
    }

    clearCanvas() {
        let ctx = this.canvas.getContext('2d');
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    handleCollisions() {
        let leftLineHorDup = this.duplicateOnHorisontalCollision(this.leftLine);
        if (leftLineHorDup) {
            if (
                leftLineHorDup.left > 0
                && (leftLineHorDup.left + leftLineHorDup.width) <= this.canvas.width
            ) {
                this.leftLine = leftLineHorDup;
            }
            else {
                this.duplicateOnVerticalCollision(leftLineHorDup);
            }
        }
        let leftLineVerDup = this.duplicateOnVerticalCollision(this.leftLine);
        if (
            leftLineVerDup
            && leftLineVerDup.top > 0
            && (leftLineVerDup.top + leftLineVerDup.height) <= this.canvas.height
        ) {
            this.leftLine = leftLineVerDup;
        }

        let rightLineHorDup = this.duplicateOnHorisontalCollision(this.rightLine);
        if (rightLineHorDup) {
            if (
                rightLineHorDup.left > 0
                && (rightLineHorDup.left + rightLineHorDup.width) <= this.canvas.width
            ) {
                this.rightLine = rightLineHorDup;
            }
            else {
                this.duplicateOnVerticalCollision(rightLineHorDup);
            }
        }
        let rightLineVerDup = this.duplicateOnVerticalCollision(this.rightLine);
        if (
            rightLineVerDup
            && rightLineVerDup.top > 0
            && (rightLineVerDup.top + rightLineVerDup.height) <= this.canvas.height
        ) {
            this.rightLine = rightLineVerDup;
        }

        let topLineHorDup = this.duplicateOnHorisontalCollision(this.topLine);
        if (topLineHorDup) {
            if (
                topLineHorDup.left > 0
                && (topLineHorDup.left + topLineHorDup.width) <= this.canvas.width
            ) {
                this.topLine = topLineHorDup;
            }
            else {
                this.duplicateOnVerticalCollision(topLineHorDup);
            }
        }
        let topLineVerDup = this.duplicateOnVerticalCollision(this.topLine);
        if (
            topLineVerDup
            && topLineVerDup.top > 0
            && (topLineVerDup.top + topLineVerDup.height) <= this.canvas.height
        ) {
            this.topLine = topLineVerDup;
        }
    }

    duplicateOnHorisontalCollision(rect) {
        let alternateRect = null;
        if (rect.left < 0) {
            let alternateLeft = this.canvas.width + rect.left;
            alternateRect = new Rect(this.canvas, alternateLeft, rect.top, rect.width, rect.height);
        }
        else if ((rect.left + rect.width) > this.canvas.width) {
            let alternateLeft = rect.left - canvas.width;
            alternateRect = new Rect(this.canvas, alternateLeft, rect.top, rect.width, rect.height);
        }

        return alternateRect;
    }

    duplicateOnVerticalCollision(rect) {
        let alternateRect = null;

        if (rect.top < 0) {
            let alternateTop = this.canvas.height + rect.top;
            alternateRect = new Rect(this.canvas, rect.left, alternateTop, rect.width, rect.height);
        }
        else if (rect.top + rect.height > this.canvas.height) {
            let alternateTop = rect.top - this.canvas.height;
            alternateRect = new Rect(this.canvas, rect.left, alternateTop, rect.width, rect.height);
        }

        return alternateRect;
    }
}

let canvas = document.querySelector('canvas');

let logo = new Logo(canvas);

document.body.addEventListener('keydown', event => {
    switch (event.key) {
        case 'ArrowLeft':
        case 'a':
            logo.moveLeft();
            break;
        case 'ArrowRight':
        case 'd':
            logo.moveRight();
            break;
        case 'ArrowUp':
        case 'w':
            logo.moveUp();
            break;
        case 'ArrowDown':
        case 's':
            logo.moveDown();
            break;
    }
})
