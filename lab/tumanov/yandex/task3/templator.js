(function() {
    class Templator {
        constructor(template) {
            this._template = template;
        }

        compile(ctx) {
            let template = this._template;
            for (let placeholderName in ctx) {
                let placeholderValue = '';
                switch (typeof ctx[placeholderName]) {
                    case 'object':
                        let regex = new RegExp(`{{\\s*${placeholderName}\\.(\\w*)\\s*}}`)
                        let match = regex.exec(template);

                        if (
                            ! match
                            || ! match[1]
                        ) {
                            break;
                        }

                        placeholderValue = ctx[placeholderName];

                        placeholderName = `${placeholderName}.${match[1]}`;

                        let path = match[1].split('.');

                        for (let i = 0; i < path.length; i++) {
                            let key = path[i];
                            if (placeholderValue[key]) {
                                placeholderValue = placeholderValue[key];
                            }
                            else {
                                placeholderValue = undefined;
                                break;
                            }
                        }
                        break;
                    case 'function':
                        window[placeholderName] = ctx[placeholderName];
                        placeholderValue = `window.${placeholderName}()`;
                        break;
                    default:
                        placeholderValue = ctx[placeholderName];
                }

                template = template.replace(new RegExp(`{{\\s*${placeholderName}\\s*}}`), placeholderValue);
            }

            return template;
        }
    }

    window.Templator = Templator;
})();
