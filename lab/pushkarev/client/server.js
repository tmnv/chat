const https = require("https");
const http = require("http");
const fs = require("fs");
const path = require("path");

const PROJECT_DIR = "/home/serg";
const ROOT_DIR = PROJECT_DIR + "/chat/lab/pushkarev/client";
const TLS_DIR = PROJECT_DIR + "/tls";

const HOST = "localhost";
const HTTP_PORT = 8080;
const HTTPS_PORT = 8443;

const options = {
    key: fs.readFileSync(path.join(TLS_DIR, "localhost_private.key")),
    cert: fs.readFileSync(path.join(TLS_DIR, "localhost_public.crt"))
};

const server = https.createServer(options, (req, res) => {
    const writeHead = () => {
        let fileExtensionWithDot = path.extname(requestedResource);

        let contentType = "";

        if (fileExtensionWithDot === "") {
            console.log(
                `У запрошенного ресурса ${requestedResource} нет расширения.`
                );
            return;
        }

        let fileExtension = fileExtensionWithDot.slice(1);

        switch (fileExtension) {
            case "html":
            case "css":
                contentType = `text/${fileExtension}; charset=utf-8`;
                break;
            case "js":
                contentType = "application/javascript; charset=utf-8";
                break;
            case "jpeg":
            case "jpg":
            case "png":
                fileExtension = fileExtension === "jpg" ? "jpeg" : fileExtension;
                contentType = "image/" + fileExtension;
                break;
            case "ico":
                contentType = "image/x-icon";
                break;
            case "mp3":
                contentType = "audio/mp3";
                break;
            default:
                console.log(
                    `Запрошен ресурс ${requestedResource} с неизвестным расширением ${fileExtension}`
                    );
        }

        res.writeHead(200, {
            "Content-Type": contentType,
        });
    };

    const sendResponse = (err, content) => {
        if (err) {
            console.log(err);
            res.writeHead(404, {
                "Content-Type": "text/html; charset=utf-8",
            });
            res.end("<h1>Файл не найден</h1>");
            return;
        }

        writeHead();

        res.end(content);
    };

    let requestedResource = "";

    switch (req.url) {
        case "/":
            requestedResource = "/page/plan.html";
            break;
        case "/chat":
        case "/list":
        case "/soul":
            requestedResource = `/page${req.url}.html`;
            break;
        default:
            requestedResource = req.url;
    }

    requestedResource = path.normalize(requestedResource);

    fs.readFile(ROOT_DIR + requestedResource, sendResponse);
});

const httpServer = http.createServer((req, res) => {
    let location = `https://${HOST}:${HTTPS_PORT}${req.url}`;

    res.writeHead(301, {
        Location: location,
});
    res.end();
});

server.listen(HTTPS_PORT, () => {
    console.log(`HTTPS-сервер запущен и слушает порт ${HTTPS_PORT}.`);
});

httpServer.listen(HTTP_PORT, () => {
    console.log(`HTTP-сервер запущен и слушает порт ${HTTP_PORT}.`);
});
