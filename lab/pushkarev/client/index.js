const express = require('express')
const path = require('path')

const app = express()

app.get('/', (req, res) => {
    res.status(200)
    res.sendFile(path.join(__dirname, 'page/index.html'))
})

const port = process.env.PORT || 8081

app.listen(port, () => {
    console.log(`Сервер слушает порт ${port}.`)
})
