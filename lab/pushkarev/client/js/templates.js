document.querySelector('header').innerHTML = `
<nav style="display: flex; align-items: center;">
    <div style="flex-grow: 1;">
    <a href="/">План развития</a>
    <a href="/chat">Чат</a>
    <a href="/list">Список участников</a>
    <a href="/soul">Психология</a>
    </div>
    <div>
    <button onclick="showModal('block')" style="display: flex; align-items: center;"><img src="/img/login.png"/><span>Регистрация</span></button>
    </div>
</nav>
<div id="filter" onclick="showModal('none')">
</div>
<div id="modalForm" style="display: none;">
    <form action="">
        <h1 title="Форма регистрации">Регистрация</h1>
        <div class="group">
            <label for="login" style="display: inline-block; width: 30%;">Имя пользователя</label>
            <input id="login" type="text">
        </div>
        <div class="group">
            <label for="password" style="display: inline-block; width: 30%;">Пароль</label>
            <input id="password" type="password">
        </div>
        <div class="group" style="margin-left: 30%;">
            <button>Зарегистрироваться</button>
        </div>
    </form>
</div>`

{/* <div id="loginForm" style="display:none; position:fixid; top:200px; left:20px; border:1px solid black;">
    <div class="close" >Закрыть</div><br/>
    <div class="submit">Действие</div>
</div> */}

// <script> document.addEventListener('DOMContentLoaded', (event) => {
//     let loginEl = document.getElementById("login")
//     loginEl.addEventListener ("click", event => {
//         let loginConditionForm = document.getElementById("loginForm")
//         loginConditionForm.style.display = "block"
//     })

// })
// </script>`
