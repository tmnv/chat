// Получаем элемент содержащий статус подключения к WebSocket.
let statusEl
// В этом элементе пользователь вводит текст сообщения.
let messageEl
// В этот элемент попадают новые сообщения.
let messageListEl

document.addEventListener('DOMContentLoaded', (event) => {
    // console.log('Страница загружена')
    //console.log(event)
    let btn = document.getElementById('send')
    btn.addEventListener('click', handleSendClick)

    // Получаем элемент содержащий статус подключения к WebSocket.
    statusEl = document.getElementById('status')
    // В этом элементе пользователь вводит текст сообщения.
    messageEl = document.getElementById('messageText')
    messageEl.addEventListener('keydown', handleKeydown)
    // В этот элемент попадают новые сообщения.
    messageListEl = document.getElementById('messageList')
})

//todo: Эти настройки должен указывать сам пользователь. Сейчас они "вшиты". Это хардкод, которого надо избегать.
const wsScheme = 'ws'
// const wsHost = 'chat.universo.su'
const wsHost = 'localhost'
const wsPort = 3000
const wsSocket = new WebSocket(`${wsScheme}:/${wsHost}:${wsPort}`)

const setStatus = value => {
    statusEl.innerHTML = value
}

const handleSendClick = event => {
    // Кнопка может быть и ссылкой. Поэтому надо предотвратить её поведение по умолчанию.
    // Иначе может произойти переход по ссылке.
    event.preventDefault()

    sendMessageToServer(messageEl.value)
}

const handleKeydown = event => {
    console.log(event.key)
    if (event.key === 'Enter') {
        // Если не отменить перенос, то после(!) отправки сообщения и очистки формы перенос будет
        // добавлен автоматически.
        event.preventDefault()
        sendMessageToServer(messageEl.value)
    }
}

const sendMessageToServer = messageText => {
    if (messageText.trim() !== '') {
        wsSocket.send(messageText)
    }

    // Стираем старое сообщение в текстовом элементе и возвращаем ему фокус (кнопка при нажатии получает фокус).
    messageEl.value = ''
    messageEl.focus()
}

const addMessageToList = messageText => {
    // Создаём элемент для нового сообщения и помещаем в него написанную строку текста.
    let newMessageEl = document.createElement('div')
    newMessageEl.classList.add('message')
    newMessageEl.innerHTML = messageText

    // Вставляем новое сообщение в список
    messageListEl.appendChild(newMessageEl)
}

wsSocket.onopen = () => setStatus('Сервер ПОДКЛЮЧЕН')
wsSocket.onclose = () => setStatus('Сервер ОТКЛЮЧЕН')
wsSocket.onmessage = response => addMessageToList(response.data)
