const http = require('http')
const fs = require('fs')
const path = require('path')

let ROOT_DIR = '/var/www/su/tehnokom/chat/client/'

const server = http.createServer((req, res) => {
    if (
        req.url === '/'
        || req.url === '/index.html'
    ) {
        res.writeHead(200, {
            "Content-Type": "text/html; charset=utf-8"
        })

        fs.readFile(path.join(
            __dirname, 'index.html'),
            'utf-8',
            (err, content) => {
                if (err) {
                    // Пока ничего не делаем.
                }

                res.end(content)
            }
        )
    }
    else if (req.url === '/script.js') {
        res.writeHead(200, {
            "Content-Type": "application/javascript; charset=utf-8"
        })
        fs.readFile(path.join(
            __dirname, 'script.js'),
            'utf-8',
            (err, content) => {
                if (err) {
                    // Пока ничего не делаем.
                }

                res.end(content)
            }
        )
    }
    else if (req.url === '/style.css') {
        res.writeHead(200, {
            "Content-Type": "text/css; charset=utf-8"
        })
        fs.readFile(path.join(
            __dirname, 'style.css'),
            'utf-8',
            (err, content) => {
                if (err) {
                    // Пока ничего не делаем.
                }

                res.end(content)
            }
        )
    }
    else if (req.url === '/lenin-i-kommunizm.jpg') {
        res.writeHead(200, {
            "Content-Type": "image/jpeg"
        })
        fs.readFile((ROOT_DIR + 'lenin-i-kommunizm.jpg'), (err, content) => {
            if (err) {
                throw err
            }

            res.end(content)
        })
    }
    else if (req.url === '/favicon.ico') {
        res.writeHead(200, {
            "Content-Type": "image/x-icon"
        })
        fs.readFile((ROOT_DIR + 'favicon.ico'), (err, content) => {
            if (err) {
                throw err
            }

            res.end(content)
        })
    }
    else {
        //console.log('Запрошен ресурс неизвестный ресурс ' + req.url + '.')
    }
})

let port = 8080
server.listen(port, () => {
    console.log('Сервер запущен и слушает порт ' + port + '.')
})
