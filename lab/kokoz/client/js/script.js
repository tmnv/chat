// Получаем элемент содержащий статус подключения к WebSocket.
let statusEl, settingsBtnEl, settingsEl
// В этом элементе пользователь вводит текст сообщения.
let messageEl
// В этот элемент попадают новые сообщения.
let messageListEl

let settingsSchemaEl, settingsHostEl, settingsPortEl
let settingsSaveBtn

let incomingMessageSound

// Настройки по умолчанию
let settings = {
    'schema': 'ws',
    'host': 'localhost',
    'port': '3000'
}

let ws

document.addEventListener('DOMContentLoaded', (event) => {
    let btn = document.getElementById('send')
    btn.addEventListener('click', handleSendClick)

    // Получаем элемент содержащий статус подключения к WebSocket.
    statusEl = document.getElementById('status')
    settingsBtnEl = document.getElementById('settingsBtn')
    settingsBtnEl.addEventListener('click', handleSetBtnClick)
    // В этом элементе пользователь вводит текст сообщения.
    messageEl = document.getElementById('messageText')
    messageEl.addEventListener('keydown', handleKeydown)
    // В этот элемент попадают новые сообщения.
    messageListEl = document.getElementById('messageList')
    //добавили слушатель аудио-тега
    incomingMessageSound = document.getElementById("incomingMessage")

    let settingsString = localStorage.getItem('settings')

    if (settingsString !== null) {
        settings = JSON.parse(settingsString);

        if (settings) {
            ws = new WebSocket(`${settings.schema}://${settings.host}:${settings.port}`)
            ws.onopen = () => setStatus('Сервер ПОДКЛЮЧЕН')
            ws.onclose = () => setStatus('Сервер ОТКЛЮЧЕН')
            ws.onmessage = response => {
                addMessageToList(response.data);
                incomingMessageSound.play();
            }
        }
    }
    settingsSchemaEl = document.getElementById('schema');
    settingsSchemaEl.value = settings.schema;
    settingsHostEl = document.getElementById('host');
    settingsHostEl.value = settings.host;
    settingsPortEl = document.getElementById('port');
    settingsPortEl.value = settings.port;

    settingsSaveBtn = document.getElementById('save');
    settingsSaveBtn.addEventListener('click', handleSaveClick);
});

const handleSetBtnClick = event => {
    event.preventDefault();

    settingsEl = document.getElementById('settings')
    if (settingsEl.style.display === 'none') {
        settingsEl.style.display = 'block'
    }
    else {
        settingsEl.style.display = 'none'
    }
}

const handleSaveClick = event => {
    event.preventDefault()

    settings = {
        'schema': settingsSchemaEl.value,
        'host': settingsHostEl.value,
        'port': settingsPortEl.value
    }

    localStorage.setItem('settings', JSON.stringify(settings))

    ws.close()

    ws = new WebSocket(`${settings.schema}://${settings.host}:${settings.port}`)
    console.log('ws.readyState = ' + ws.readyState)

    ws.onerror = event => {
        console.error("Ошибка подключения:", event)
        settingsEl.style.backgroundColor = 'red'
    }

    ws.onopen = () => {
        setStatus('Сервер ПОДКЛЮЧЕН')
        settingsEl.style.backgroundColor = 'green'
        setTimeout(() => {
            settingsEl.style.display = 'none'
        }, 3000)

    }
    ws.onclose = () => setStatus('Сервер ОТКЛЮЧЕН')
    ws.onmessage = response => {
        console.log(response)

        addMessageToList(response.data)
        incomingMessageSound.play()
    }
}

const setStatus = value => {
    statusEl.innerHTML = value
}

const handleSendClick = event => {
    // Кнопка может быть и ссылкой. Поэтому надо предотвратить её поведение по умолчанию.
    // Иначе может произойти переход по ссылке.
    event.preventDefault()

    sendMessageToServer(messageEl.value)
}

const handleKeydown = event => {
    // console.log(event.key)
    if (event.key === 'Enter') {
        // Если не отменить перенос, то после(!) отправки сообщения и очистки формы перенос будет
        // добавлен автоматически.
        event.preventDefault()
        sendMessageToServer(messageEl.value)
    }
}

const sendMessageToServer = messageText => {
    let usernameEl = document.getElementById('username')
    username = usernameEl.value.trim()

    if (username === '') {
        alert('Укажите своё имя.')
        usernameEl.focus()
        return
    }

    try {
        if (messageText.trim() === '') {
            alert('Напишите сообщение.')
            messageEl.focus()
            return
        }

        let message = {
            name: username,
            text: messageText
        }

        // Пакуем сообщение в строку и отправляем его на WS сервер.
        let packedJson = JSON.stringify(message)
        console.log()
        ws.send(packedJson)

        // Стираем старое сообщение в текстовом элементе и возвращаем ему фокус (кнопка при нажатии получает фокус).
        messageEl.value = ''
        messageEl.focus()

    }
    catch (e) {
        console.log(e)
    }
}

const formatDate = date => {
    return date.getFullYear() + '-' + (date.getMonth() + 1).toString().padStart(2, '0') + '-' + date.getDate()
        + ' ' + date.getHours().toString().padStart(2, '0') + ':' + date.getMinutes() + ':' + date.getSeconds()
}

const addMessageToList = messageText => {
    let message = JSON.parse(messageText)

    // Создаём элемент для нового сообщения и помещаем в него написанную строку текста.
    let newMessageEl = document.createElement('div')
    newMessageEl.classList.add('message')

    let dateEl = document.createElement('div')
    dateEl.classList.add('message__date')
    dateEl.innerHTML = formatDate(new Date(message.date))

    newMessageEl.appendChild(dateEl)

    let nameEl = document.createElement('div')
    nameEl.classList.add('message__name')
    nameEl.innerHTML = message.name

    newMessageEl.appendChild(nameEl)

    let textEl = document.createElement('div')
    textEl.classList.add('message__text')
    textEl.innerHTML = message.text

    newMessageEl.appendChild(textEl)

    // Вставляем новое сообщение в список
    messageListEl.appendChild(newMessageEl)
}
