const http = require('http')
const https = require('https')
const fs = require('fs')
const path = require('path')

const PROJECT_DIR = '/var/www/su/tehnokom'
const ROOT_DIR = PROJECT_DIR + '/chat/client'
const TLS_DIR = PROJECT_DIR + '/tls'

const HOST = 'learn.tehnokom.su'
const HTTP_PORT = 80
const HTTPS_PORT = 443

const options = {
    "key": fs.readFileSync(path.join(TLS_DIR, `${HOST}_private.key`)),
    "cert": fs.readFileSync(path.join(TLS_DIR, `${HOST}_public.crt`))
}

const server = https.createServer(options, (req, res) => {
    let requestedResource = ''

    switch (req.url) {
        case '/':
            requestedResource = '/page/plan.html'
            break
        case '/chat':
        case '/list':
            requestedResource = `/page${req.url}.html`
            break
        default:
            requestedResource = req.url
    }

    requestedResource = path.normalize(requestedResource)

    const writeHead = () => {
        let fileExtensionWithDot = path.extname(requestedResource)

        let contentType = ''

        if (fileExtensionWithDot === '') {
            console.log(`У запрошенного ресурса ${requestedResource} нет расширения.`)
            return
        }

        let fileExtension = fileExtensionWithDot.slice(1)

        switch(fileExtension) {
            case "html":
            case "css":
                contentType = "text/" + fileExtension + "; charset=utf-8"
                break
            case "js":
                contentType = "application/javascript; charset=utf-8"
                break
            case "jpeg":
            case "jpg":
            case "png" :
                fileExtension = (fileExtension === 'jpg') ? "jpeg" : fileExtension
                contentType = "image/" + fileExtension
                break
            case "ico":
                contentType = "image/x-icon"
                break
            case "mp3":
                contentType = "audio/mp3"
                break
            default:
                console.log(`Запрошен ресурс ${requestedResource} с неизвестным расширением ${fileExtension}`)
        }

        res.writeHead(200, {
            "Content-Type": contentType
        })
    }

    const sendResponse = (err, content) => {
        if (err) {
            console.log(err)
            res.writeHead(404, {
                "Content-Type": "text/html; charset=utf-8"
            })
            return
        }

        writeHead(requestedResource)

        res.end(content)
    }

    fs.readFile(
        (ROOT_DIR + requestedResource),
        sendResponse
    )
})

const httpServer = http.createServer((req, res) => {
    let location = `https://${HOST}:${HTTPS_PORT}${req.url}`

    res.writeHead(301, {
        'Location': location
    })
    res.end()
})

server.listen(HTTPS_PORT, () => {
    console.log(`HTTPS-сервер запущен и слушает порт ${HTTPS_PORT}.`)
})

httpServer.listen(HTTP_PORT, () => {
    console.log(`HTTP-сервер запущен и слушает порт ${HTTP_PORT}.`)
})
