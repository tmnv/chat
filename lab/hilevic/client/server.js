const https = require('https')
const fs = require('fs')
const path = require('path')

const PROJECT_DIR = '/var/www/su/tehnokom'
// let ROOT_DIR = PROJECT_DIR + '/client'
const ROOT_DIR = PROJECT_DIR + '/chat/lab/hilevic/client'
const TLS_DIR = PROJECT_DIR + '/tls'

const options = {
    "key": fs.readFileSync(path.join(TLS_DIR, 'chat_private.key')),
    "cert": fs.readFileSync(path.join(TLS_DIR, 'chat_public.crt'))
}

const server = https.createServer(options, (req, res) => {
    let requestedResource = ''

    switch (req.url) {
        case '/':
            requestedResource = '/page/index.html'
            break
        case '/plan':
            requestedResource = '/page/plan.html'
            break
        case '/list':
            requestedResource = '/page/list.html'
            break
        default:
            requestedResource = req.url
    }

    requestedResource = path.normalize(requestedResource)

    const writeHead = () => {
        let fileExtensionWithDot = path.extname(requestedResource)

        let contentType = ''

        if (fileExtensionWithDot === '') {
            console.log(`У запрошенного ресурса ${requestedResource} нет расширения.`)
            return
        }

        let fileExtension = fileExtensionWithDot.slice(1)

        switch(fileExtension) {
            case "html":
            case "css":
                contentType = "text/" + fileExtension + "; charset=utf-8"
                break
            case "js":
                contentType = "application/javascript; charset=utf-8"
                break
            case "jpeg":
            case "jpg":
            case "png" :
                fileExtension = (fileExtension === 'jpg') ? "jpeg" : fileExtension
                contentType = "image/" + fileExtension
                break
            case "ico":
                contentType = "image/x-icon"
                break
            case "mp3":
                contentType = "audio/mp3"
                break
            default:
                console.log(`Запрошен ресурс ${requestedResource} с неизвестным расширением ${fileExtension}`)
        }

        res.writeHead(200, {
            "Content-Type": contentType
        })
    }

    const sendResponse = (err, content) => {
        if (err) {
            console.log(err)
            res.writeHead(404, {
                "Content-Type": "text/html; charset=utf-8"
            })
            return
        }

        writeHead(requestedResource)

        res.end(content)
    }

    fs.readFile(
        (ROOT_DIR + requestedResource),
        sendResponse
    )
})

let port = 8443
server.listen(port, () => {
    console.log('Сервер запущен и слушает порт ' + port + '.')
})
