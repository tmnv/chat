/* FizzBuzz - изначально, детская игра для развития навыков счета
Требуется написать функцию, выводящую в консоль числа от 1 до n, 
где n — это целое число, которая функция принимает в качестве параметра, 
с такими условиями:
вывод fizz вместо чисел, кратных 3;
вывод buzz вместо чисел, кратных 5;
вывод fizzbuzz вместо чисел, кратных как 3, так и 5. */

Fizzbuzz(5)
// 1
// 2
// fizz
// 4
// buzz
// ..
// 14
// fizzbuzz
// 16


// Решение 1. Классический if
let fizzbuzz = num => {
  for (i = 0; i <= num; i++) {
    if (i % 3 == 0 && i % 5 == 0) { //Важно! fizzbuzz должен стоить первым:
      console.log("fizzbuzz");
    } else if (i % 3 == 0) {
      console.log("fizz");
    } else if (i % 5 == 0) {
      console.log("buzz");
    } else {
      console.log(i);
    }
  }
}

fizzbuzz(100);


//Решение 2. Через swith
let fizzbuzz = num => {
  for (i = 1; i <= num; i++) {
    switch (0) {
      case ((i % 3 == 0) && (i % 5 == 0) ? 0 : 1):
        console.log("fizzbuzz");
        break;
      case (i % 3):
        console.log("fizz");
        break;
      case (i % 5):
        console.log("buzz");
        break;
      default:
        console.log(i);
        break;
    }
  }
}

fizzbuzz(100);


//Решение 3. Через тернарный оператор
const fizzbuzz = num => {
  for (i = 1; i <= num; i++) {
    const threes = i % 3;
    const fives = i % 5;
    // Общий вид тернарного оператора:    a == 10 ? "Равно" : "Не равно"
    // В качестве действия после проверки в тернарном операторе может быть еще один тернарный оператор
    console.log((threes === 0 && fives === 0) ? 'FizzBuzz' : (threes === 0 && fives !== 0) ? 'Fizz' : (fives === 0) ? 'Buzz' : i);
  }
}

fizzbuzz(100);