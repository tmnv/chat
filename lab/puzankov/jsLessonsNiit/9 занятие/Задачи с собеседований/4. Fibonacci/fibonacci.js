/* Фибоначчи
Последовательность Фибоначчи — это ряд чисел, 
где каждое последующее является суммой двух предыдущих. 
Так, первые десять чисел выглядят следующим образом: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34.

Лекция TED https://www.youtube.com/watch?v=kayX8XTjL9E
On-line энциклопедия целочисленных последовательностей https://oeis.org/?language=russian

ЗАДАЧА
Создать функцию, которая возвращает n-ную запись 
в последовательности чисел Фибоначчи */
fibonacci(7) // --> 13



//Решение 1. Классический цикл
const fibonacci = num => {
  const result = [0, 1]
  for (let i = 2; i <= num; i++) {
    const prevNum1 = result[i - 1]
    //console.log(prevNum1 + "-1st")
    const prevNum2 = result[i - 2]
    //console.log(prevNum2 + "-2nd")
    //console.log(result + ' \n')
    result.push(prevNum1 + prevNum2)
  }
  
  return result[num];
  
}
fibonacci(5);

//Решение 2. Рекурсивный вызов фукнции
const fibonacci = num => {
  if (num < 2) {
    return num;
  }
  //console.log(num -1 )
  return fibonacci(num - 1) + fibonacci(num - 2);
}

fibonacci(7);
//Мы продолжаем вызывать fibonacci(), передавая все меньшие числа в качестве аргументов. 
//Останавливаемся в случае, когда переданный аргумент равен 0 или 1.