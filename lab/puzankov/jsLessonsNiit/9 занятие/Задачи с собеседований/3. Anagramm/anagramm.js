/* Анаграмма - слово, которое содержит все буквы другого слова в том же количестве, 
но ином порядке.

Лунь - нуль     Воз - зов    Чертог - горечь   Днесь - снедь

ЗАДАЧА
Нужно написать функцию, которая проверяет, являются ли две строки анаграммами, 
причем регистр букв не имеет значения. Учитываются лишь символы; 
пробелы или знаки препинания в расчет не берутся. */

anagramm('finder', 'Friend') -- > "Анаграммы" //эти два слова анаграммы
anagramm('hello', 'goodbye') -- > "Не анаграммы" //это не анаграммы



//Решение 1. Классические циклы и массивы
let anagramm = (word1, word2) => {

  //приводим слова в нижний регистр
  let lettersWord1 = word1.toLowerCase().replace(/[^a-zа-яё]/g, '').split('');
  let lettersWord2 = word2.toLowerCase().replace(/[^a-zа-яё]/g, '').split('');
  //Сравниваем длину слов, если разная - не анаграммы
  if (lettersWord1.length != lettersWord2.length) {
    console.log("Слова разной длины, не анаграммы!");
  } else {
    //Хранение длины слов
    let wordsLength = lettersWord1.length;

    //Создаем и заполяняем два массива для хранения информации о количестве каждой буквы в словах
    let literalsСount1 = [];
    let literalsСount2 = [];
    for (i = 0; i < wordsLength; i++) {
      literalsСount1[i] = 0;
    }
    for (i = 0; i < wordsLength; i++) {
      literalsСount2[i] = 0;
    }
    //Массивы для хранения пар БУКВА:КОЛИЧЕСТВО
    let result1 = [];
    let result2 = [];
    //Считаем количество каждого символа 1 слова
    for (i = 0; i < wordsLength; i++) {
      for (j = 0; j < wordsLength; j++) {
        if (lettersWord1[i] == lettersWord1[j]) {
          literalsСount1[i]++;
        }
      }
      result1[i] = lettersWord1[i] + literalsСount1[i];
    }
    //Считаем количество каждого символа 2 слова
    for (i = 0; i < wordsLength; i++) {
      for (j = 0; j < wordsLength; j++) {
        if (lettersWord2[i] == lettersWord2[j]) {
          literalsСount2[i]++;
        }
      }
      result2[i] = lettersWord2[i] + literalsСount2[i];
    }

    let matchLetters = 0; //счетчик количества совпавших пар буква:количество для двух слов
    for (i = 0; i < wordsLength; i++) {
      //ищем пару буква:количество первого слова во втором (в итоговых массивах)
      let index = result2.indexOf(result1[i]);
      //если пара не найдена, в другом слове такой буквы нет или она в другом количестве
      if (index == -1) {
        console.log("Не анаграммы!");
        break;
      } else {
        matchLetters++;
        if (matchLetters == wordsLength) {
          console.log("Анаграммы!");
        }
      }
    }
  }
};

anagramm("ракета", "ратаке");


//Решение 2. Использование литерального объекта

//Для хранения данных анаграммы стоит выбрать такую структуру, как объектный литерал JavaScript. 
//Ключ в этом случае — символ буквы, значение — количество ее повторений в текущей строке.

ракета-- >
р: 1
а: 2
к: 1
е: 1
т: 1

// всппомогательная функция создания объекта для хранения данных
// в ней в цикле считаем количество каждого символа с создаем объект
// со свойствами буква:количество
let buildCharObject = str => {
  let charObj = {}
  for (let char of str.toLowerCase().replace(/[^a-zа-яё]/g, '')) {
    charObj[char] = charObj[char] + 1 || 1
  }
  return charObj
}

// главная функция
let anagram = (strA, strB) => {
  // создание объекта из первого и второго слова
  let aCharObject = buildCharObject(strA)
  let bCharObject = buildCharObject(strB)

  // Сравнение длины слов, для анаграмм она одинаоквая
  if (Object.keys(aCharObject).length !== Object.keys(bCharObject).length) {
    return false
  }
  // Сравнение количества каждого символа в словах
  for (let char in aCharObject) {
    if (aCharObject[char] !== bCharObject[char]) {
      return false
    }
  }
  return true
}