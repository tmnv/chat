// LocalStorage - локальное хранилище

// LocalStorage - это хранилище данных, встроенное в движок браузера и
// позволяющие приложения хранить данные без потери их при закрытии вкладки и самого браузера
// Объем хранимой информации ограничен 5 мб.
// Работа с LS основывается на стандарте JSON (Java Script Object Notation)
// Это формат хранения данных в виде КЛЮЧ - ЗНАЧЕНИЕ

{
  "firstName": "Иван",
  "lastName": "Иванов",
  "address": {
    "streetAddress": "Московское ш., 101, кв.101",
    "city": "Ленинград",
    "postalCode": 101101
  },
  "phoneNumbers": [
    "812 123-1234",
    "916 123-4567"
  ]
}


//Обычный объект JS
let myCar = {
  wheels: 4,
  doors: 4,
  engine: 1,
  name: "Jaguar"
}

//То же самое но в формате JSON
{
  "wheels": 4,
  "doors": 4,
  "engine": 1,
  "name": "Jaguar"
}


//VS Code есть удобный плагин для JSON - JSON Tools 
//Он позволяет форматировать Ctrl(Cmd)+Alt+M и минифицировать Alt+M
//Демоданыеё https://github.com/awesomedata/awesome-public-datasets

//LocalStorage - это свойство глобального объекта браузера (window). 
//К нему можно обращаться как window.localStorage или просто localStorage.

//Также у браузера существует клон localStorage, который называется sessionStorage. 
//Разница только в том, что последний хранит данные только для одной вкладки 
//(сессии) и просто очистит свое пространство как только мы закроем вкладку

//ВАЖНО! Для каждого домена ваш браузер создает свой объект localStorage, 
//и редактировать или просматривать его можно только на этом домене. 
//Например, с домена mydomain-1.com нельзя получить доступ 
//к localStorage вашего mydomain-2.com. */



//Основные методы работы LocalStorage

//Добавляем или изменяем значение:
//если такой ключ уже существует, то перезаписывает новым значением.
localStorage.setItem('myKey', 'myValue'); //теперь у вас в localStorage хранится ключ "myKey" cо значением "myValue"

//Выводим элемент в консоль:
let localValue = localStorage.getItem('myKey');
console.log(localValue); //"myValue"

//удаляем элемент:
localStorage.removeItem("myKey");

//очищаем все хранилище
localStorage.clear()

//То же самое, только с квадратными скобками:
localStorage["Ключ"] = "Значение" //установка значения
localStorage["Ключ"] // Получение значения
delete localStorage["Ключ"] // Удаление значения




// Сериализация и парсинг JSON
// Сериализация (в программировании) — процесс перевода какой-либо структуры данных 
// в последовательность битов. Обратной к операции сериализации является операция десериализации (парсинг)
// (структуризации) — восстановление начального состояния структуры данных из битовой последовательности. */

//создадим объект
let obj = {
  item1: 1,
  item2: [123, "two", 3.0],
  item3: "hello"
};

let serialObj = JSON.stringify(obj); //сериализуем его
localStorage.setItem("myKey", serialObj); //запишем его в хранилище по ключу "myKey"
let returnObj = JSON.parse(localStorage.getItem("myKey")) //спарсим его обратно объект


// Браузер выделяет 5мб под localStorage. 
// И если вы его превысите — получите исключение QUOTA_EXCEEDED_ERR. 

try {
  for (let i = 0; i < 500000; i++) {
    localStorage.setItem(i, "Элемент");
  }
} catch (e) {
  if (e == QUOTA_EXCEEDED_ERR) {
    alert('Превышен лимит 5 мб');
  }
}