// 30. Работа с ошибками, try catch

//https://learn.javascript.ru/try-catch

try {
    // код в котором может произойти ошибка
} catch (err) {
    // код если ошибка произошла
}

/* 
1. Сначала выполняется код внутри блока try {...}.
2. Если в нём нет ошибок, то блок catch(err) игнорируется: выполнение доходит до конца try 
   и потом далее, полностью пропуская catch.
3. Если же в нём возникает ошибка, то выполнение try прерывается, 
   и поток управления переходит в начало catch(err). 
   Переменная err (можно использовать любое имя) содержит объект ошибки с подробной информацией о произошедшем.

Для всех встроенных ошибок этот объект имеет два основных свойства:
name - Имя ошибки. Например, для неопределённой переменной это "ReferenceError".
message - Текстовое сообщение о деталях ошибки.
В большинстве окружений доступны и другие, нестандартные свойства. 
Одно из самых широко используемых и поддерживаемых – это:
stack - Текущий стек вызова: строка, содержащая информацию о последовательности 
вложенных вызовов, которые привели к ошибке. Используется в целях отладки.
   */


try {
    alert('Начало блока try'); // (1) <--
    // ...код без ошибок
    alert('Конец блока try'); // (2) <--
} catch (err) {
    alert('Catch игнорируется, так как нет ошибок'); // (3)
}

try {
    alert('Начало блока try'); // (1) <--
    lalala; // ошибка, переменная не определена!
    alert('Конец блока try (никогда не выполнится)'); // (2)
} catch (err) {
    alert(`Возникла ошибка!`); // (3) <--
}

/* Таким образом, try..catch может обрабатывать только ошибки, которые возникают 
в корректном коде. Такие ошибки называют «ошибками во время выполнения», 
а иногда «исключениями». */

try {
    //{{{{}}} какой-то непонятный код
} catch (e) {
    alert("Движок не может понять этот код, он некорректен");
}

//Реальный пример испльзования try catch

let json = '{"name":"John", "age": 30}'; // данные с сервера
let user = JSON.parse(json); // преобразовали текстовое представление в JS-объект
// теперь user - объект со свойствами из строки
alert(user.name); // John
alert(user.age); // 30

/* 
Если json некорректен, JSON.parse генерирует ошибку, то есть скрипт «падает».
Устроит ли нас такое поведение? Конечно нет!
Получается, что если вдруг что-то не так с данными, 
то посетитель никогда (если, конечно, не откроет консоль) об этом не узнает. 
А люди очень не любят, когда что-то «просто падает» без всякого сообщения об ошибке.
*/


let json = "{ некорректный JSON }";
try {
    let user = JSON.parse(json); // <-- тут возникает ошибка...
    alert(user.name); // не сработает
} catch (e) {
    // ...выполнение прыгает сюда
    alert("Извините, в данных ошибка, мы попробуем получить их ещё раз.");
    alert(e.name);
    alert(e.message);
}

/* Конструкция try..catch может содержать ещё одну секцию: finally.
Если секция есть, то она выполняется в любом случае:
после try, если не было ошибок,
после catch, если ошибки были. */

try {
    alert('try');
    if (confirm('Сгенерировать ошибку?')) BAD_CODE();
} catch (e) {
    alert('catch');
} finally {
    alert('finally');
}

