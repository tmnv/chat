//################################################################################################################################
//###############   31.Работа с датами и временем в JS   ############################################################################
//################################################################################################################################

// UTC - это CUT(Coordinated Universal Time) Всемирное координированное время
// В UTC нет перехода на летнее и зимнее время
// GMT - время по Гринвичу, накапливало ошибки из-за замедления вращения Земли
// UTC - основывается на атомном времени (но все равно каждый код добавляется 1сек)
// На планете 24 часовых пояса, точка отсчета - Гринвичский мередиан (UTC 0)
// Слева от него - отриц. значения UTC, справа - положительные 
// Москва UTC+3, а Нью-Йорк UTC-5 (летом UTC-4)
// Пояса UTC сравнительно равномерны, однако часто проходят по границам государств
// Если страна небольшая, но немного “залезает” краями соседние часовые пояса, 
// на ее территории вводится единый формат времени. 
// На территории России действуют 11 часовых поясов по UTC: 
// от UTC+2 в Калининграде до UTC+12 на Камчатке.
// Материал по датам в JS https://habr.com/ru/company/mailru/blog/438286/

Формат записи времени по UTC
YYYY - MM - DD hh: mm: ss ZZZ(2013 - 07 - 16 20: 13: 31 + 01)

//В JS для работы с датами применяется глобальный объект Date
//Экземпляр объекта Date представляет текущий момент времени.  
//Несмотря на имя Date, он также обрабатывает время и часовой пояс
//Два разных компьютера могут выводить разные значения 
//для одного и того же объекта даты если они находятся в разных часовых поясах
//плагинин VS Code для вставки текущего времени - Insert Date String cmd shift I

//Создадим объект Date, указывающий на текущий момент времени
//в него запишется метка времени в моменты вызова
new Date();

// Глобальный объект Data() в JS - это специальный класс типа данных объект
// для хранения данных о дате и времени, поэтому хранит данные об этих параметрах
// и корректо их обрабатывать можно только через него.

// Получить текущее время (timestamp) можно так
new Date().getTime() //1581154027388 в миллисекундах
Date.now() // Sat Feb 08 2020 12:27:46 GMT+0300 (Moscow Standard Time)



// Задать время для объекта Date 4 способами:


// 1. Не передавать параметров, создать объект Date, который представляет текущую дату и время
const currentTime = new Date();
// Важно отметить что в большинстве случает дата сохраняется в константу, и это понятно 


// 2. передача числа, которое представляет миллисекунды с 1 января 1970 года
// Внутренне даты выражаются в миллисекундах с 1 января 1970 года (UTC).
// Отметка времени UNIX: количество секунд , прошедших с 1 января 1970 г.
// Калькулятор для перевода https://www.timecalculator.net/milliseconds-to-date
// В JS текущее время в СЕКУНДАХ обозначается timestamp
let timestamp = 1530826365;
new Date(timestamp * 1000);


// 3. передача строки c датой и временем в Data()
new Date('2018-07-22')
new Date('2018-07') //July 1st 2018, 00:00:00
new Date('2018') //Jan 1st 2018, 00:00:00
new Date('07/22/2018')
new Date('2018/07/22')
new Date('2018/7/22')
new Date('July 22, 2018')
new Date('July 22, 2018 07:22:13')
new Date('2018-07-22 07:22:13')
new Date('2018-07-22T07:22:13')
new Date('25 March 2018')
new Date('25 Mar 2018')
new Date('25 March, 2018')
new Date('March 25, 2018')
new Date('March 25 2018')
new Date('March 2018') //Mar 1st 2018, 00:00:00
new Date('2018 March') //Mar 1st 2018, 00:00:00
new Date('2018 MARCH') //Mar 1st 2018, 00:00:00
new Date('2018 march') //Mar 1st 2018, 00:00:00


// 4. передача набора параметров, которые представляют различные части даты
new Date(2018, 6, 22, 7, 22, 13, 0)

// Data() будет использовать метод parse и разбирать строку и получать дату
Date.parse('2018-07-22T07:22:13'); //вернет отметку timestamp в мс

//Добавление часового пояса
new Date('July 22, 2018 07:22:13 +0700');



// После того как дата создана, с ней можно работать в программе
// Вывод строки с датой
const date = new Date();
date.toString() // "Sun Jul 22 2018 07:22:13 GMT+0200 (Central European Summer Time)"
date.toTimeString() //"07:22:13 GMT+0200 (Central European Summer Time)"
date.toUTCString() //"Sun, 22 Jul 2018 05:22:13 GMT"
date.toDateString() //"Sun Jul 22 2018"
date.toISOString() //"2018-07-22T05:22:13.000Z" (ISO 8601 format)
date.toLocaleString() //"22/07/2018, 07:22:13"
date.toLocaleTimeString() //"07:22:13"
date.getTime() //1532236933000
date.getTime() //1532236933000

//Получение отдельных частей даты и времени (по местрому времени)
const date = new Date('July 22, 2018 07:22:13')
date.getDate() //22
date.getDay() //0 (0 means sunday, 1 means monday..)
date.getFullYear() //2018
date.getMonth() //6 (starts from 0)
date.getHours() //7
date.getMinutes() //22
date.getSeconds() //13
date.getMilliseconds() //0 (not specified)
date.getTime() //1532236933000
date.getTimezoneOffset() //-180 - отставиние времени UTC от текущего в минутах

//получение значений времени UTC (время по Гринвичу UTC00,а не текущего часового пояса)
date.getUTCDate() //22
date.getUTCDay() //0 (0 means sunday, 1 means monday..)
date.getUTCFullYear() //2018
date.getUTCMonth() //6 (starts from 0)
date.getUTCHours() //5 (not 7 like above)
date.getUTCMinutes() //22
date.getUTCSeconds() //13
date.getUTCMilliseconds() //0 (not specified)

//Редатирование даты
const date = new Date('July 22, 2018 07:22:13')
date.setDate(newValue)
date.setDay(newValue)
date.setFullYear(newValue) //метод setYear() устаревший, не применяется
date.setMonth(newValue)
date.setHours(newValue)
date.setMinutes(newValue)
date.setSeconds(newValue)
date.setMilliseconds(newValue)
date.setTime(newValue)
date.setTimezoneOffset(newValue);
// ВАЖНО, методы взаимосвязаны, то есть он также будет увеличивать день.
date.setHours(48)



//////////////// ПРАКТИЧЕСКИЕ ПРИМЕРЫ///////////////////

// 1. Сравнение двух дат
const date1 = new Date('July 10, 2018 07:22:13')
const date2 = new Date('July 10, 2018 07:22:13')
if (date2.getTime() === date1.getTime()) {
    console.log("Даты совпадают");
}

// 2. Сравнение дат. Мы можем вычи��ать даты, результат будет в миллесекундах
// Также можно коенчно складывать множать и делить но в этом уже нет смысла
const date1 = new Date('July 10, 2018 07:22:13')
const date2 = new Date('July 22, 2018 07:22:13')
const diff = date2.getTime() - date1.getTime();
const diffDays = new Date(diff);
console.log("Разница между датами в мс:" + diff);
console.log("Разница между датами в днях:" + diffDays.getDay());

/* Важно иметь в виду, что метод getTime() возвращает количество миллисекунд, 
поэтому при сравнении необходимо учитывать время. July 10, 2018 07:22:13 не равно July 10, 2018. 
В этом случае можно использовать метод setHours(0, 0, 0, 0) для сброса времени. */
date1.setHours(0, 0, 0, 0) //uly 10, 2018 00:00:00



// 3. Вывести текущую дату в формате "Сегодня: 7 Февраля 2020, Пятница"
// Необходимы методы:
const myDate = new Date(); //Получение текущего времени
myDate.getDate()
myDate.getMonth()
myDate.getFullYear()
myDate.getDay()

let days = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
let months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
const myDate = new Date();
let fullDate = "Сегодня: " + myDate.getDate() + " " + months[myDate.getMonth()] + " " + myDate.getFullYear() + ", " + days[myDate.getDay()];
console.log(fullDate); // Сегодня: 18 Август 2015, Вторник




// 4. Приветсвие пользователя в зависимости от времени суток 
// "Добрый вечер (утро, день), текущее время: 22:50:39 "

let welcome;
const myDate = new Date();
let hour = myDate.getHours();
let minute = myDate.getMinutes();
let second = myDate.getSeconds();

if (minute < 10) {
    minute = "0" + minute;
}
if (second < 10) {
    second = "0" + second;
}

if (hour < 12) {
    welcome = "Доброе утро";
} else if (hour < 17) {
    welcome = "Добрый день";
} else {
    welcome = "Добрый вечер";
}

console.log(welcome + ", текущее время: " + hour + ":" + minute + ":" + second);

