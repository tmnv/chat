let addMessage = document.querySelector('.input_add');
let addNew = document.querySelector('.add_new');
let todo = document.querySelector('.todo');

let todoList = [];



if(localStorage.getItem('todolist')){
    todoList = JSON.parse(localStorage.getItem('todolist'));
    displayMessages ();
}

addNew.addEventListener ('click', function(){
   
    let newTodo = {
        task: addMessage.value,
        checked: false,
    };
   
    if (addMessage.value === '') {
        alert('Введите вашу задачу')
    } else {
        todoList.push(newTodo);
        displayMessages ();
        localStorage.setItem('todolist', JSON.stringify(todoList));
    }

    

});

function displayMessages (){
    
    let displayMessage = '';
    todoList.forEach(function(item, i){
        displayMessage += `
        <li>
            <input type='checkbox' id='item_${i}' ${item.checked ? 'checked' : ''}>
            <label for='item_${i}'>${item.task}</label>
        </li>
        `;
        todo.innerHTML = displayMessage;
    });   
}


todo.addEventListener('change', function(event){
    let valueLabel = todo.querySelector('[for='+ event.target.getAttribute('id') +']').innerHTML;

    todoList.forEach(function(item){
        if(item.task === valueLabel){
            item.checked = !item.checked;
            localStorage.setItem('todolist', JSON.stringify(todoList));
        }
    })
});
