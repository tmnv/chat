//генерируем игровое поле
//место для будущей функции генерации игрового поля
let stepCount = 0; // счетчик ходов

let player = 'X'; //текущий игрок

let winCombination = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];                  //выйгрышные комбинации

//массивы для значений ячеек по которым нажал игрок
let dataCellX = [];
let dataCellO = [];

//список из всех клеток игрового поля
let cells = document.querySelectorAll('.game-item');
console.log(cells);
//находим кнопку сброса игры
let resetGame = document.querySelector('#reset-game');
console.log(resetGame);
let message = document.querySelector('#message');
console.log(message);

//добавляем обработчик событий на все ячейки
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', currentStep);
}

//ход игрока
function currentStep() {
    //заисываем знчение атриба которое мы получаем при клетки на поле.
    let num = +this.getAttribute('data-cell');
    //запускаем проверку, есть ли уже значение в клетке по которой мы кликнули.
    if (!this.textContent) {
        this.innerText = player;
        (player === "X") ?
            dataCellX.push(num) :
            dataCellO.push(num);
        //проверка
        if (
            (dataCellX.length > 2 || dataCellO.length > 2) &&
            (isWinner(dataCellX, num) || isWinner(dataCellO, num))
        ) {
            //удаляем обработчик из клеток
            for (let i = 0; i < cells.length; i++) {
                cells[i].removeEventListener('click', currentStep);
            }
            //меняем сообщение на странице
            return (message.innerText = 'Победил игрок ' + player);

        }
        //смена текущего игрока
        changePlayer();
        stepCount++;
        (stepCount === 9) ? (message.innerText = 'Ничья') :
            (message.innerText = 'Ходит игрок ' + player)
    }
}


//смена ходя игрока
function changePlayer() {
    player === 'X' ? (player = 'O') : (player = 'X');
}

//сброс поля
resetGame.addEventListener('click', function () {
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerText = "";
    }
    //сброс результатов игроков
    dataCellO = [];
    dataCellX = [];
    //сброс значения текущего игрока
    player = 'X';
    //сброс счечика ходов
    stepCount = 0;
    //меняем сообщение о ходе текущего игрока
    message.innerText = 'Ходит игрок ' + player;
    
    //снова добавляем обработчик
    for (let i = 0; i < cells.length; i++) {
        cells[i].addEventListener('click', currentStep);
    }
})

//arr - массив результатов хода игрока; number - значение аргумента data-cell
//поиск победителя
function isWinner(arr, number) {
    for (let i = 0, iLeng = winCombination.length; i < iLeng; i++) {
        let winArr = winCombination[i];
        let count = 0;
        if (winArr.indexOf(number) !== -1) {
            for (let j = 0, jLeng = winArr.length; j < jLeng; j++) {
                if (arr.indexOf(winArr[j]) !== -1) {
                    count++;
                    if (count === 3) {
                        return true;
                    }
                }
            }
            count = 0;
        }
    }
}