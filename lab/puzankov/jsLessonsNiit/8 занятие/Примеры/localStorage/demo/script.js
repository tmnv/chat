let item1 = document.querySelector("#item1");
let item2 = document.querySelector("#item2");
let sendButton = document.querySelector("#send");
let clearButton = document.querySelector("#clear");

item1.value = null;
item2.value = null;

function addItems() {
  if (!(item1 == null || item2 == null)) {
    localStorage.setItem(item1.value, item2.value);
    item1.value = null;
    item2.value = null;
  }
}

sendButton.addEventListener("click", addItems);

clearButton.addEventListener("click", function () {
  localStorage.clear();
});