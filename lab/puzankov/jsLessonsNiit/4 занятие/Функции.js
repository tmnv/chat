// 21. Функции

//Cпособы создания функций:

//1. Function Declaration: функция в основном потоке кода
function sayHello(name) {
  console.log('Hello, ' + name)
}

//2. Function Expression: функция как часть выражения
let sayHello = function (name) {
  console.log('Hello, ' + name)
}

//3. Стрелочные фукнции
Стрелочные функции:
// выражение в правой части
let sum = (a, b) => a + b;


//когда функция создана первыйм способом - 
//при запуске файла парсер сначала обрабатывает все функции и потом запускает код
//следующий код будет выдавать ошибку так как на момент вызова самой функции еще не будет создано
sayHello('Маша')
let sayHello = function (name) {
  console.log('Hello, ' + name)
}

//а так работать будет
let sayHello = function (name) {
  console.log('Hello, ' + name)
}
sayHello('Маша')



//"Возвращение" фукнций
//Функции всегда что-нибудь возвращают. Если нет оператора return, 
//результатом будет undefined:
//функция без параметра, возвращает undefined
function hello() {
  console.log('Hello world!')
}

//фукнция с параметром, возвращает undefined
function hello(name) {
  console.log('Привет, ' + name + '!')
}

//функция, которая возвращает значение
function avg(a, b, c) {
  let result = (a + b + c) / 3
  return result
}


//Функция рассчета факториала
function fact (n) {
  let result = 1;
  for (let i = 1; i <= n; i++) {
    result = result * i;
  }
  return result;
}

fact(5);



function myFirstApp(name, age) {

  // приветствие
  alert(`Привет! Меня зовут ${name}`);

  // показываем наши навыки
  function showSkills() {
      let skills = ["html", "css", "js"];
      for (let i = 0; i < skills.length; i++) {
          console.log(`Я владею ${skills[i]}`);
      }
  }

  showSkills();



  // проверяем возраст
  function chekAge() {
      if (age >= 18) {
          alert("Ты будешь крутым фронтэндером!");
      } else {
          alert("Круто, что ты так рано задумался о своем будущем.");
      }
  }

  chekAge();



  // функция возведения в квадрат
  function calcPow(num) {
      return Math.pow(num, 2);
  }

  console.log(calcPow(4));

}

// вызов главной функции
myFirstApp("Витя", 31);





let carYear = 2015;
let personYear = 2005;

// запись нужного функционала через IF
if ((2019 - carYear) < 10)
  console.log('Возраст машины меньше 10 лет');
else
  console.log('Возраст машины больше либо равен 10 годам');

if ((2019 - personYear) < 10)
  console.log('Возраст человека меньше 10 лет');
else
  console.log('Возраст человека больше либо равен 10 годам');


// запись такой же логики через фукнции

//Функция вычисляет возраст
function calculateAge(year) {
  //внутри функции мы можем создавать нужные нам переменные для работы
  let currentYear = 2019
  let result = currentYear - year
  //ключевое слово return означает что функция возвращает что-то (отдает результат работы)
  return result
}

//Можно вызывать одну фукнцию внутри другой
//Функция выводит сообщения в зависимости от возраста
function checkAndLogAge(year) {
  if ((calculateAge(year)) < 10) {
      console.log('Возраст меньше 10 лет')
  } else {
      console.log('Возраст больше 10 лет')
  }
}

//Вызов функций для разных объектов
checkAndLogAge(carYear)
checkAndLogAge(personYear)