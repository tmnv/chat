// 20. Оператор FOR EACH

let names = ["Маша", "Катя", "Оля"];

// обычный цикл for
for (i = 0; i < names.length; i++) {
    alert(names[i]);
};

// оператор for each
/* 
item - каждый элемент в перебираемом массиве
i - индекс перебираемого элемента
array -  имя массива, который мы перебираем */

let boys = ["Сергей", "Михаил", "Борис", "Александр"];
boys.forEach(function (item, i, array) {
    if (i < 2) {
        console.log(item);
    }
});
