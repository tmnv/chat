// 12. Массивы

let cars = ['Ford', 'Mazda', 'Kia', 'BMW']
console.log(cars)

//массив может содержать любые типы данных
let any = [42, 'Apple', {a: 2}]
console.log(any)

console.log(cars[2]) //вывод элемента массива по его индексу
console.log(cars.length) //вывод длины массива

cars.push('Audi') //добавление элемента в массив
console.log(cars)

let last = cars.pop() //метод pop удаляет и возвращает последний элемент в массиве
console.log(cars, last)

let first = cars.shift() //удаление и возврат первого элемента массива
console.log(cars, first)

cars.unshift(last) //добавление первого элемента массива
console.log(cars)

let index = cars.indexOf('Kia') //поиск индекса по содержимому (точно указанному)
console.log(index)

positiveNumbers = numbersArray.filter(function(num){ //фильтрация элементов
    return num>0;
})


// includes возвращает true, если переданное значение нашлось в массиве, 
// и false, если его там нет.

let stations = [ "Salyut", "Skylab", "Almaz", "Mir", "ISS", "Tiangong" ]
stations.includes("Skylab"); > true
stations.includes("Space Station V"); > false

//includes находит только целый элемент, а не вхождение в строку:
stations.includes("Sky"); > false

//Поиск регистрозависимый:
stations.includes("skylab"); > false

//можно задать стартовую точку поиска при помощи необязательного аргумента:
stations.includes("Mir", 2); > true
stations.includes("Mir", 5); > false

//Из-за отсутствия в JS четко выраженной типизации данных метод includes можно применять 
//и к строкам для нахождения слов, фраз и последовательностей символов.
let sentence = "The ISS orbits at a height of 400km";
sentence.includes("orbits"); > true

[1, 2, 3].includes(2);     // true
[1, 2, 3].includes(4);     // false
[1, 2, 3].includes(3, 3);  // false
[1, 2, 3].includes(3, -1); // true
[1, 2, NaN].includes(NaN); // true



//push - pop    unshift  shift
//  +     -        +       -
//   конец          начало