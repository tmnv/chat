// 23. События и обработчики

// Нежелательное использование обработчиков:
// <button id = "hello" onclick = "sayHello()" > Show hello! < /button>

let button = document.querySelector("#search");
button.addEventListener("click", sayHello, true);

function sayHello() {
    alert("Hello!");
}


//Удаление обработчика события

let button = document.getElementById('button');

button.addEventListener('click', func); //назначаем событие

function func() {
	alert('Нажатие на кнопку!');
	button.removeEventListener('click', func); //после клика удаляем его
}