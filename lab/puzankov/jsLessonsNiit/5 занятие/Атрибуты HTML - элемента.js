// 26. Атрибуты HTML - элемента

let link = document.querySelector('a')

console.log(link.getAttribute('href')); //1 способ - получение аттрибута через метод
console.log(link.attributes.href); //2 способ - получение аттрибутов через массив attributes

link.setAttribute('href', 'https://yandex.ru') //установка значения аттрибута с помощью метода
