// 24. Получение HTML - элементов в JS

//Устаревшие методы получения доступа к HTML из JS
let test = document.getElementById('playground')
let p = document.getElementsByClassName('.text')
let h1 = document.getElementsByTagName('h1')

console.log(div);
console.log(p);
console.log(h1);*/


//Актуальный метод
// class -> .CLASS_NAME
// id -> #ID_NAME
// tag -> TAG_NAME

let div = document.querySelector('#playground') //по id
let p = document.querySelectorAll('.text') //по классу, выводится NodeList
let h1 = document.querySelector('h1') //по тэгу, пишем просто тэг
let hello = document.querySelector('#hello div ul')
//можно гибко строить запросы на выборку данных, как в CSS

console.log(div)
console.log(p);
console.log(h1);
console.log(hello);