// 25. Содержание элементов, внутренний контент

//вывод содержимого элемента (в виде текста)
let hello = document.querySelector('#hello')
console.log(hello.innerHTML);

//innerHTML это содержимое элемента, html код и контент
console.log(hello);
hello.innerHTML = '<h2 style = "color:red">Привет из JS!</h2>'

//Свойство TextContent позволяет менять содержимое тэга, сам контент
let h1 = document.querySelector('h1')
h1.textContent = 'Привет, Новый Мир!'
console.log(h1.textContent);

//Свойство value позволяет получить доступ к содержимому input
let userName = document.querySelector('input')
console.log(input.value);