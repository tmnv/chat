// 15. Условный оператор IF

let carName = 'Ford'
let carYear = 2001
let currentYear = 2018
let carAge = currentYear - carYear

//есл в.м. меньше 5 лет
//если в.м. больше 5 но меньше 10 лет
//Возраст машины $carAge$ лет

let age = 70;
if (age > 65) {
    console.log("Пора на пенсию");
} else {
    console.log("Еще не пора на пенсию");
}


if (carAge < 5) {
    console.log(carName + ' младше 5 лет')
} else if (carAge >= 5 && carAge <= 10) {
    console.log('Возраст ' + carName + ' от 5 до 10 лет включительно')
} else if {
    console.log('Возраст ' + carName + ':' + carAge + ' лет')
}





/* 

0 
null 
undefined     при проверке через if получаем FALSE
'' 
NaN

 */

//NaN - Not a namber, не число, получается например при делении на 0. Тип NaN - number

let str = ''

if (str) {
    console.log('Значение true')
} else {
    console.log('Значение false')
}