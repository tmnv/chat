// 9. Дополнительные арифметические операции

//Остаток от деления
console.log('10 % 5', 10 % 5) //0
console.log('10 % 3', 10 % 3) //1
//проверка четности
console.log('10 % 2', 10 % 2) //0 - число четное, если 1 - нечетное

let i = 1
//Инкремент Декремент
i++
i--

i += 5
i -= 5
i *= 2
i /= 2

//Постфиксный инкремент декремент, если делать вывод то сначала получим значение, потом операция
i++
i--

//Префиксный инкремент декремент, сначала операция потом вывод
++i
--i
console.log(i);
