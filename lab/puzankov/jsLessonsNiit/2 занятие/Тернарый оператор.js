// 17. Тернарый оператор

//стандратный вариант if
let age = 17

if (age >= 18) {
    console.log('Совершеннолетний')
} else {
    console.log('Несовершеннолетний')
}

//тернарный оператор, простая проверка
let age = 18
//2 формы записи, первая простоя проверка
age >= 18 ? console.log('Совершеннолетний') : console.log('Несовершеннолетний')


//проверка и присваивание, оператор возвращает значение в зависимости от условия
let message = age >= 18 ? 'Есть 18' : 'Нет 18'
console.log(message)

let vasyaLove = 'Маша'
let vasyaGirlFriend = vasyaLove == 'Маша' ? "Маша" : "Кто-то еще, но не Маша"
console.log(vasyaGirlFriend)

//еще один пример тернарного оператора
let a = 26
let message = a == 25 ? 'Число а равно 25' : 'число а не равно 25'
console.log(message)