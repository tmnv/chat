// 14. Логические операторы сравнения

//Строго
console.log('5 > 3', 5 > 3)
console.log('4 < 4', 4 < 4)

//Нестрого
console.log('12 >= 12', 12 >= 12)
console.log('12 > 12', 12 > 12)

//Равенство
console.log('4 == 4', 4 == 4)
console.log('4 == 5', 4 == 5)

//Неравенство
console.log('4 != 4', 4 != 4)
console.log('4 != 5', 4 != 5)

//Проверка на тип данных и значение
console.log('4 === 4', 4 === '4') //false
console.log('4 == 4', 4 == '4') // true

console.log('4 !== 4', 4 !== '4') //true
console.log('4 !== 4', '4' !== '4') //false