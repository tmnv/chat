// 6. Числа
console.log(42)
console.log(-42)
console.log(1.5)
console.log(-8 / 3) //-2.6666... в периоде
console.log(0xAB) //171 в шестнадцатиричной системе
console.log(2e3) //будет 2000, 2 - основание, 3 = 10 в степени 3
console.log(NaN) //NaN - not a number
console.log(typeof (NaN)) //number числовой тип
console.log(2 / 'hello') //будет NaN
console.log(123 / 0) //Infinity - бесконечность

let fortyTwo = 42
let delta = 8 / 3

console.log(fortyTwo.toString()) //перевод числа в строку
console.log(delta.toFixed(1)) //округление числа, в скобках - сколько знаков после запятой
console.log(+delta.toFixed(1)) //привидение строки к числу с помощью знака +
console.log(delta.toFixed(1) + 4) //вернет 2.74
console.log(+delta.toFixed(1) + 4) //вернет 6.7
console.log(parseInt(delta.toFixed(2))) //2
console.log(parseFloat(delta.toFixed(2))) //2.67
console.log(isNaN(NaN)) //проверка является ли что-то NaN
console.log(isNaN(45)) //это не NaN
console.log(isFinite(1 / 0)) //false проверка является ли что-то бесконечным
console.log(isFinite(99999999999)) //true - вовзращается если это не бессконечность

