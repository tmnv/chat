// 8. Базовые арифметические операции
let num1 = 10
let num2 = 23

console.log('+:', num1 + num2)
console.log('-:', num1 - num2)
console.log('*:', num1 * num2)
console.log('/:', num1 / num2)

let str1 = 'Hello'
let str2 = 'World'

console.log('str1 + str2:', str1 + ' ' + str2) //Сложение строк - конкатенация

console.log('str1 + num1:', str1 + num1) //При сложении числа и строки получается строка
console.log('num2 + str1:', num2 + str1)
console.log('Boolean + string:', true + str1) //При сложении строки и булева значения получается строка
console.log('True + 1:', true + 1) //При сложении булева с числом получается число
console.log('False + 1:', false + 1) //true это 1, false это 0