// 10. Приоритет операций
/*
приоритеты операторов с самого высшего:
()            скобки
* /           умножение деление
+ -           сложение вычитание
> >= < <= ==  условные выражения
=             присвоение

Все приоритеты можно посмотреть здесь:
https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
*/

let result = 5 - 2 * 2
let result2 = (3 + 4) * 3

let isGreater = 3 + 6 / 3 >= 4
//приоритеты: 3  13  14   11

let isGreater2 = (3 + 6) / 3 >= 4
//приоритеты:  3    20   14  11


console.log('5 - 2 * 2:', result);
console.log('(3 + 4) * 3:', result2);
console.log('3 + 6 / 3 >= 4:', isGreater);
console.log('(3 + 6) / 3 >= 4:', isGreater2);