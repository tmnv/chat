// 5. Строки

//ЭКРАНИРОВАНИЕ СИМВОЛОВ
//Вывод кавычек и обратного слэша
let name = 'Виктор'
let message = 'Человека зовут ' + name
let message2 = 'Человека зовут "' + name + '"'
let message3 = "Человека зовут '" + name + "'"
//Если нужно вывести кавычку - ее экранирует слэшом
let message4 = 'Человека зовут \\\'' + name + '\'' //первый слэш экранирует второй, третий экранирует кавычку
console.log(message4)

//работа со строками в JavaScript
let newMessage = 'Добро пожаловать на борт корабля!!!'
console.log(newMessage); //простой вывод

console.log(newMessage.length); //получение длины строки
console.log(newMessage.toUpperCase()); //приведение строки в верхний регистр
console.log(newMessage.toLowerCase()); //приведение в нижний

console.log(newMessage.charAt(4)); //вывод отдельного символа с указанным индексом (нумерация с 0!)

console.log(newMessage.indexOf('бро')); //получение номера первого символа указанной строки
console.log(newMessage.indexOf('cat')); //то же самое, вернет -1, так как фрагмент не найден

console.log(newMessage.substr(5, 10)); //вывод фрагмента по начальному индексу и длине
console.log(newMessage.substr(newMessage.indexOf('жало'), 8)); //получение начального идекса и вывод
console.log(newMessage.substring(7)); //вывод строки с указанного индекса и до конца
console.log(newMessage.substring(3, 10)); //вывод строки от и до указанных индексов

//Реверсирование строки
let letters = "Поляризация";
let reverseLetters =  letters.split('').reverse().join('');

//Поиск и замена в строке
let words = "Меня зовут Маша";
words2 = words.replace(/Маша/g,"Оля");


// В JS есть кавычки
/* 
одинарные ''  
двойные  ""  
обратные (backtics) ``
   */

//Расширение для автоматической смены типа кавычек - Toggle Quotes   ctrl '

// c помощью обтных кавыечек можно выводить значения переменных непосредственно через alert prompt и др. элементы
// этот процесс назывется интерполяция

alert(`Привет! Меня зовут ${name} и мне ${age} лет.`);