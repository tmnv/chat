// 19. Циклы

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let letters = ['A', 'B', 'C', 'D']
let badArray = [1, 2, 3, 4, 5, 6, 'hello', 7, 8, 9, 10]
//индексы       0   1   2   3  4  5   6       7  8  9    10


//Простой вывод элементов массива через цикл
for (let i = 0; i < numbers.length; i++) {
    console.log(numbers[i]);
}


//Вывод только четных чисел, 1 способ
for (let i = 0; i < numbers.length; i++) {
    if ((numbers[i] % 2) === 0) {
        console.log(numbers[i]);
    }
}

//Вывод только четных чисел, 2 способ - continue
for (let i = 0; i < numbers.length; i++) {
    if ((numbers[i] % 2) !== 0) {
        continue; //когда доходим до оператора continue - происходит сразу переход на следующий шаг
    }
    console.log(numbers[i]);
}

//Вывод только числовых элементов массива, после первой строки - выход
//оператор break применяется для прерывания цикла
for (let i = 0; i < badArray.length; i++) {
    if ((typeof (badArray[i]) !== 'number')) {
        break;
    }
    console.log(badArray[i]);
}


//Вывод только четных элементов массива из букв
for (let i = 0; i < letters.length; i++) {
    if ((i % 2) !== 0) {
        continue;
    }
    console.log(letters[i]);
}

//бесконечный цикл
for (;;) {
    // будет выполняться вечно
}
//или так
for(i=1; i>0;i++)
{
    console.log("Бесконечный цикл");
}



//цикл while (Цикл ПОКА с ПРЕДУСЛОВИЕМ)
while (condition) {
    // код
    // также называемый "телом цикла"
}

let i = 0;
while (i > 10) {
    console.log(i);
    i++;
}

let i = 0;
while (i < 3) { // выводит 0, затем 1, затем 2
    alert(i);
    i++;
}




//цикл do while (Цикл ДО с ПОСТУСЛОВИЕМ) чтобы цикл выполнился хотя бы один раз
do {
    // тело цикла
} while (condition);


let i = 0;
do {
    console.log(i);
    i++;
} while (i < 10);


//запись через for
let answers = [],
    questions = [
        "Ваше имя?",
        "Ваша фамилия?",
        "Ваш возраст?"
    ];

for (let i = 0; i < questions.length; i++) {
    answers[i] = prompt(questions[i], '');
}

document.write(answers);


//та же логика через while
let answers = [],
    questions = [
        "Ваше имя?",
        "Ваша фамилия?",
        "Ваш возраст?"
    ];

let i = 0;
while (i > 10) {
    answers[i] = prompt(questions[i], '');
    i++;
}

document.write(answers);


//for...of
https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/for...of

Традиционный for цикл в JavaScript - теперь в прошлом! Встречайте новые циклы for..of и for…in, которые дают нам очень ясный и короткий синтаксис для последовательного прохода по всем типам итерируемых и перечисляемых значений, таких как строки, массивы и объектные литералы. Поэтому, данная статья будет неким подобием шпаргалки, к которой можно обращаться, чтобы освежить в памяти новые функции JavaScript!

for…of
Используйте данный цикл for…of для итерации по значениям в итерируемых типах, например массиве:

 
  let animals = ['кошка', 'собака', 'лев', 'тигр'];
  let names = ['Гертруда', 'Генри', 'Мэлвин', 'Билли'];

  for(let animal of animals)
  {
   // Случайное имя для кошки
   let nameIdx = Math.floor(Math.random() * names.length);
   console.log(`${names[nameIdx]}- ${animal}`);
  }
Возможнрый результат

 
  // Генри - собака
  // Мэлвин - лев
  // Генри - собака
  // Билли - тигр

Строки также являются итерируемыми типами, поэтому вы можете использовать JavaScript цикл for…of для строк:

 
  let str = 'abcde';

  for (let char of str)
  {
   console.log(char.toUpperCase().repeat(3));
  }

  // AAA
  // BBB
  // ...
Вы также можете перебирать карты (map), наборы (set), генераторы (generator), коллекции узлов DOM и объект arguments, доступный внутри каждой JavaScript функций.

for…in
Используйте этот цикл для то, чтобы перебирать свойства объекта (ключи объекта) в JavaScript:


  let oldCar = {

   make: 'Tesla',
   model: 'S',
   year: '2014'

  };

  for (let key in oldCar)
  {
   console.log(`${key} --> ${oldCar[key]}`);
  }

  // make --> Tesla
  // model --> S
Вы также можете использовать for…in для итерации по индексам перебираемого объекта, например массива или строки:

 
  let str = 'Turn the page';

  for (let index in str)
  {
   console.log(`Индекс ${str[index]}: ${index}`);
  }
Результат:


  // Индекс T: 0

  // Индекс u: 1






  Оба оператора, и for...in и for...of производят обход объектов. Разница лишь в том, как они это делают.

for...of
Оператор for...of выполняет обход по элементам коллекций (иначе говоря, итерируемых объектов, например, Array, Map, Set, String, arguments, DOM коллекций и т.д.), вызывая на каждом шаге итерации значение, а не ключ.

Оператор for...of для массивов работает аналогично forEach, но имеет преимущества: возможность использовать continue и break для контроля итераций, что невозможно в forEach.

Обход массива
let arr = [10, 20, 30];

for (let value of arr) {
  value += 1;
  console.log(value);
}
// 11 21 31
Обход строки
let str = 'cold';

for (let value of str) {
  console.log(value);
}
// "c" "o" "l" "d"
Обход Map
let iterable = new Map([['a', 1], ['b', 2], ['c', 3]]);

for (let entry of iterable) {
    console.log(entry);
}
// ['a', 1] ['b', 2] ['c', 3]

for (let [key, value] of iterable) {
    console.log(value);
}
// 1 2 3
for...in
Цикл for...in проходит через перечисляемые свойства объекта. Он пройдёт по каждому отдельному элементу, вызывая на каждом шаге ключ. Цикл for...in проходит по свойствам в произвольном порядке, поэтому его не следует использовать для Array.

var obj = {a:1, b:2, c:3};

for (var prop in obj) {
  console.log(obj[prop]);
}

// 1 2 3    

https://ua-blog.com/%D1%86%D0%B8%D0%BA%D0%BB%D0%B8-for-in-%D0%B8-for-of-%D0%B2-js/






Оператор for...of выполняет цикл обхода итерируемых объектов (включая Array, Map, Set, объект аргументов и подобных), вызывая на каждом шаге итерации операторы для каждого значения из различных свойств объекта.

for (переменная of объект)
  оператор
переменная
На каждом шаге итерации переменной присваивается значение нового свойства объекта.
объект
Объект, свойства которого обходятся во время выполнения цикла.

Обход Array
let iterable = [10, 20, 30];

for (let value of iterable) {
  value += 1;
  console.log(value);
}
// 11
// 21
// 31

Обход Array
let iterable = [10, 20, 30];

for (let value of iterable) {
  value += 1;
  console.log(value);
}
// 11
// 21
// 31

Различия между for...of и for...in
Оба оператора, и for...in и for...of производят обход объектов . Разница в том как они это делают.

Для for...in обход перечисляемых свойств объекта осуществляется в произвольном порядке.

Для for...of обход происходит в соответствии с тем, какой порядок определен в итерируемом объекте.

