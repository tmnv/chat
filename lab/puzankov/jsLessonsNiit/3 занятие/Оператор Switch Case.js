// 18. Оператор Switch Case

let carColor = 'orange'

//Классический if
/* if (carColor == 'green') {
  console.log('Цвет машины зеленый')
} else if (carColor == 'red') {
  console.log('Цвет машины красный')
} else if (carColor == 'orange') {
  console.log('Цвет машины оранжевый')
} else {
  console.log('Цвет машины не определен')
} */


//Оператор switch case применяется когда количество вариантов строго определено

switch (carColor) {
    case 'green':
        console.log('Цвет машины зеленый')
        break;
    case 'red':
        console.log('Цвет машины красный')
        break;
    case 'orange':
        console.log('Цвет машины оранжевый')
        break;

    default:
        console.log('Цвет машины не определен')
        break;
}


let mounthNumber = 4
switch (mounthNumber) {
    case 1:
        console.log("Январь")
        break

    case 2:
        console.log("Февраль")
        break

    case 3:
        console.log("Март")
        break

    case 4:
        console.log("Апрель")
        break

    case 5:
        console.log("Май")
        break

    case 6:
        console.log("Июнь")
        break

    case 7:
        console.log("Июль")
        break

    case 8:
        console.log("Август")
        break

    case 9:
        console.log("Сентябрь")
        break

    case 10:
        console.log("Октябрь")
        break

    case 11:
        console.log("Ноябрь")
        break

    case 12:
        console.log("Декабрь")
        break

    default:
        console.log('Неверный номер месяца')
        break
}


// оператор switch  проверяет на строгое соответствие то есть ===
// Метод prompt возвращает данные в текстовом формате
let school = prompt("В каком вы классе?", '');

switch (school) {
    case 15:
        alert("Вы учитесь в 15 школе");
        break;
    case 20:
        alert("Вы учитесь в 20 школе");
        break;
    case 25:
        alert("Вы учитесь в 25 школе");
        break;
    default:
        alert("Вы учитесь в какой-то еще школе");
}
