// Классы, прототипы, конструкторы

// В JS стандарта ES2015 и раннее реализация создания объектов на основе какого-то
// шаблона сделана через фукнции-конструкторы и прототипы
// В стандарте ES2016 появилась возможность создавать объекты через class
// Важно понимать что class в JS - является "синтаксическим сахаром", т.е. это
// элемент, который никак не влияет на ход работы программы, но существенно
// облегчает чтение и понимание кода разработчиками.
// По большому счету, class - это просто новый синтаксис для работы с
// прототипами и фукнциями - конструкторами

//Конструкторы, создание объектов через "new"
//Конструктор - Это особый метод в классе, который создает и инициализирует объект, 
//созданный с помощью класса. Это означает, что каждый раз, когда вы создаете новый экземпляр класса, 
//JavaScript автоматически вызывает метод конструктора.
//У каждого класса может быть только один конструктор. 
//Типичное использование конструктора – создание свойств класса по умолчанию. 
//Когда вы хотите добавить свойство в класс, вы делаете это в методе конструктора.
//Когда вы хотите добавить метод, вы делаете это внутри класса, но вне конструктора.
//Когда вы хотите сослаться на любое свойство или метод внутри класса, 
//вы должны использовать ключевое слово this.
//Затем вы можете передать эти свойства при создании новых экземпляров класса. 
//Или вы можете объявить их с некоторыми значениями по умолчанию.
//Метод конструктора не является обязательным. 
//Вы можете определить классы с помощью конструктора (Пример 1) или без него (Пример 2).

// Создаем новый класс под названием MyClass
class NewClass {
  // Добавляем два свойства класса, classPropOne и classPropTwo
  constructor(classPropOne, classPropTwo) {
    this.classPropOne = classPropOne;
    this.classPropTwo = classPropTwo;
  }
  // Добавляем метод класса с именем someClassMethod
  someClassMethod() {
    return this.classPropOne;
  }
  // Добавляем метод класса с именем anotherClassMethod
  anotherClassMethod() {
    return this.classPropTwo;
  }
}




class MyClass {
  // Используем конструктор для добавления свойств класса
  constructor(message = 'Hello world!') {
    this.message = message;
  }
  // Добавляем метод класса
  printMessage() {
    return this.message;
  }
}
// Создаем экземпляр класса MyClass
const instanceOfMyClass = new MyClass();
console.log(instanceOfMyClass.printMessage()); // Вывод: 'Hello world!'




class MyClass {
  // Добавляем метод класса
  printMessage() {
    return 'Hello world!';
  }
}
// Создаем экземпляр класса MyClass
const instanceOfMyClass = new MyClass();
console.log(instanceOfMyClass.printMessage()); // Вывод: 'Hello world!'



//Если вы включаете конструктор в класс, вы должны определить его в самом начале класса, вверху кода класса. 
//В противном случае JavaScript выдаст ошибку.

//Конструктор используется для установки свойств объекта в момент создания экземпляра. 
//Хотя это и похоже на другие языки, в JavaScript это просто функция.
//Фукнции конструкторы применяются когда нужно много раз создавать объекты,
//одинаковые по своей структуру, например заказы на сайте, заявки и т.п.
//По своей сути конструкторы - это обычные функции, но есть 2 особенности:
//1. Имя ф-ии начинает с Заглавной буквы
//2. При вызове функции используется ключевое слова new

// Создаем фукнцийю-конструктор для ЧЕЛОВЕКА и МАШИНЫ
// Значение this, при использовании в объекте, является сам объект.
// Другими словами, часто говорят что this - контекст вызова,
// Ключевое слова this можно заменить на имя класса

function Person(first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;
  this.sayHello = function (name) {
    console.log("Я, " + this.firstName + ", приветствую тебя," + name);
  }
}

function Car(model, number, year) {
  this.carModel = model,
    this.carNumber = number,
    this.carYear = year,
    this.carVoice = function (voice) {
      console.log(voice);
    }
}



// Создаем конкретный объект человека и машины на основе шаблона конструктора
let person1 = new Person("Александр", "Пушкин", 23, "blue");
let person2 = new Person("Николай", "Гоголь", 30, "red");
let person3 = new Person("Аркадий", "Гайдар", 50, "pink");
// также можно создать пустой объект
let person4 = new Person();

let car1 = new Car("Opel", 123456, 1987, "Bi-bi");
car1.carVoice("hello!");



// Также для отдельно экземляра мы можем добавить новое свойство (но только ему)
person1.height = 175;
// и метод
person1.sayHello = function (name) {
  console.log("Я, " + this.firstName + ", приветствую тебя," + name);
};



// Для того чтобы добавить новое свойство или метод для ВСЕХ объектов
// нужно добавить его в функции конструктор либо изначально:
function Person(first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;

  this.sayHello = function (name) {
    console.log("Я, " + this.firstName + ", приветствую тебя," + name);
  }

  this.changeName = function (name) {
    this.firstName = name;
  };

}

//либо с помощью ОБЪЕКТА - ПРОТОТИПА:
Person.prototype.gender = "Женщина";

Person.prototype.getAge = function () {
  // this относится к объекту Person
  return this.age;
}


//Кроме того в JS есть встроенные конструкторы для собственных объектов:
let x1 = new Object(); // A new Object object
let x2 = new String(); // A new String object
let x3 = new Number(); // A new Number object
let x4 = new Boolean(); // A new Boolean object
let x5 = new Array(); // A new Array object
let x6 = new RegExp(); // A new RegExp object
let x7 = new Function(); // A new Function object
let x8 = new Date(); // A new Date object

//Также для создания таких объектов можно использовать литералы:
let x1 = {}; // new object
let x2 = ""; // new primitive string
let x3 = 0; // new primitive number
let x4 = false; // new primitive boolean
let x5 = []; // new array object
let x6 = /()/ // new regexp object
let x7 = function () {}; // new function object



//Объявления классов не поднимаются наверх (not hoisted). 
//Сначала нужно объявить класс и только после этого использовать его, иначе будет ошибка ReferenceError.
//Нет необходимости использовать ключевое слово function во время задания функций внутри определения класса

// Создание класса
class Person {
  constructor(name) {
    this.name = name;
  }

  sayName() {
    console.log(`Person ${this.name} said his name`);
  }
}

const john = new Person('John');
john.sayName(); // Person John said his name










// Создадим класс машины
class Car {
  constructor(model, number, power) {
    this.carModel = model;
    this.carNumber = number;
    this.carPower = power;
  }

  carVoice(voice) {
    console.log(voice);
  }
}




//создадим новый экземпляр класса Car
let car1 = new Car("Honda", 43443, 500);





//НАСЛЕДОВАНИЕ КЛАССОВ
//Возможно создавать вложенную стрктуру классов и наследовать свойства и методы от одного класса к другому.
//В ES6 ключевое слово extends позволяет классу-потомку наследовать от родительского класса. 
//Важно отметить, что конструктор класса-потомка должен вызывать super().
//Также, в классе-потомке можно вызвать метод родительского класса с помощью super.имяМетодаРодителя().
//В JS нет никаких ограничений вложенности классов, можно сделать 100 классов наследующих друг друга
class MySubclass extends SuperClass{}

Переопределение конструтора класса
Как вы могли видеть в примерах выше, все подклассы имели свой собственный метод конструктора. 
Это означает, что они переопределяли конструктор суперкласса. Когда это происходит, 
когда подкласс переопределяет конструктор суперкласса, вы должны вызывать метод super() 
со всеми начальными параметрами конструктора.

Вызов super() внутри конструктора вызывает конструктор суперкласса, в данном случае – Vehicle. 
Это позволяет подклассам использовать свойства, определенные в конструкторе суперкласса. 
Важно помнить, что вам нужно вызывать метод super() в самой верхней части конструктора.

Вы должны вызывать его перед тем, как добавлять какие-либо свойства. 
Если вы забудете об этом, ключевое слово this и его ссылка на класс не будут существовать, 
и JavaScript выдаст ошибку. Если у подкласса нет собственного конструктора, 
тогда вам не нужно беспокоиться ни о конструкторе суперкласса, ни о super().



// Создаем суперкласс Vehicle
class Vehicle {
  constructor(name, condition, speed) {
    this.name = name;
    this.condition = condition;
    this.speed = speed;
  }
}
// Создаем подкласс Car
class Car extends Vehicle {
  constructor(name, condition, speed, numOfWheels) {
    //Вызываем функцию super() со всеми параметрами, необходимыми для класса Vehicle
    super(name, condition, speed);
    this.numOfWheels = numOfWheels;
  }
  // Добавляем метод для вывода всех свойств
  printInfo() {
    return `Название: ${this.name}, Состояние: ${this.condition}, Макс. скорость: ${this.speed}, Колес: ${this.numOfWheels}`;
  }
}
// Создаем экземпляр класса Car
const tesla = new Car('Tesla', 'новый', 280, 4);
console.log(tesla.printInfo()); // Вывод: 'Название: Tesla, Состояние: новый, Макс. скорость: 280, Колес: 4'


class Car {
  constructor() {
    console.log("Создаём новый автомобиль");
  }
}

class Porsche extends Car {
  constructor() {
    super();
    console.log("Создаём Porsche");
  }
}

let c = new Porsche();
// Создаём новый автомобиль
// Создаём Porsche



class Person{
	constructor(name, age){
		this.name = name;
		this.age = age;
	}
	display(){
		console.log(this.name, this.age);
	}
}
class Employee extends Person{
	constructor(name, age, company){
		super(name, age);
		this.company = company;
	}
	display(){
		super.display();
		console.log("Employee in", this.company);
	}
	work(){
		console.log(this.name, "is hard working");
	}
}

let tom = new Person("Tom", 34);
let bob = new Employee("Bob", 36, "Google");
tom.display();
bob.display();
bob.work();



//Статические методы
//Статические методы вызываются для всего класса вцелом, а не для отедельного объекта. 
//Для их определения применяется оператор static. Например:

class Person{
	constructor(name, age){
		this.name = name;
		this.age = age;
	}
	static nameToUpper(person){ //статический метод
		return person.name.toUpperCase();
	}
	display(){
		console.log(this.name, this.age);
	}
}
let tom = new Person("Tom Soyer", 34);
let personName = Person.nameToUpper(tom); //Вызываем метод класса, а не объекта
console.log(personName);		// TOM SOYER

//В данном случае определен статический метод nameToUpper(). 
//В качестве параметра он принимает объект Person и переводит его имя в верхний регистр. 
//Поскольку статический метод относится классу вцелом, 
//а не к объекту, то мы НЕ можем использовать в нем ключевое слово this 
//и через него обращаться к свойствам объекта.


//Класс, как и функция, является объектом. 
//Статические свойства класса User – это свойства непосредственно User, 
//то есть доступные из него «через точку».
//Для их объявления используется ключевое слово static.

'use strict';

class User {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  static createGuest() {
    return new User("Гость", "Сайта");
  }
};

let user = User.createGuest();

alert( user.firstName ); // Гость

alert( User.createGuest ); // createGuest ... (функция)
//Как правило, они используются для операций, не требующих наличия объекта, 
//например – для фабричных, как в примере выше, то есть как альтернативные варианты конструктора. 






//Геттеры и сеттеры
//Геттер - это метод который срабатывает при попытки получить какое-то свойство объекта
//Сеттер - метод, который срабатывае при попытке записать какое-то свойство
'use strict';

class User {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  // геттер
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  // сеттер
  set fullName(newValue) {
    [this.firstName, this.lastName] = newValue.split(' ');
  }

  // вычисляемое название метода
  ["test".toUpperCase()]() {
    alert("PASSED!");
  }

};

let user = new User("Вася", "Пупков");
alert( user.fullName ); // Вася Пупков
user.fullName = "Иван Петров";
alert( user.fullName ); // Иван Петров
user.TEST(); // PASSED!



//Делаем невозможным присвоение свойству пустого значения
class User {
  #nameValue;
  constructor(name) {
    this.name = name;
  }
  get name() {    
    return this.#nameValue;
  }
  set name(name) {    
    if (name === '') {
      throw new Error(`name field of User cannot be empty`);
    }
    this.#nameValue = name;
  }
}
const user = new User('Jon Snow');
user.name; // The getter is invoked, => 'Jon Snow'
user.name = 'Jon White'; // The setter is invoked
user.name = ''; // The setter throws an Error




//Проверка ЭКЗЕМПЛЯР - КЛАСС
//С помощью метода instanceof можно проверить является ли какой-то объект экзмемпляром какого-то класса
class User {
  name;
  constructor(name) {
    this.name = name;
  }
  getName() {
    return this.name;
  }
}
const user = new User('Jon Snow');
const obj = {};
user instanceof User; // => true
obj instanceof User; // => false