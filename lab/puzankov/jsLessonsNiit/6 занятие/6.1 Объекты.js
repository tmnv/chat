// 22. Объекты

//создание объекта
let person = {
  //поля (свойства объекта)
  name: 'Василий',
  year: 1990,
  family: ['Елена', 'Игорь'],
  car: {
      year: 2010,
      model: 'Ford'
  },
  //метод объекта
  calculateAge: function () {
      let age = 2020 - this.year //ключевое слово this позволяет получить доступ к свойства ЭТОГО, ТЕКУЩЕГО объекта
      console.log('Возраст ' + age)
      this.personAge = 2020 - this.year // добавление нового свойтва объекта, команда this.year = person.year
    },
  //Возраст увеличенный в x раз
  hugeNAge: function (x) {
      this.agex3 = this.personAge * x
      console.log('Возраст увеличенный в ' + x + ' раз:' + this.agex3)
  }
}

//также можно создавать новые свойства и методы объекта снаружи
person.newProp = "25"; //у объекта person создалось новое свойство со значением 25
//если имя нового свойства будет также браться как переменная и в него может попасть
//какая-то некорректная информация (например пробел) то его нужно заключить в []
let car = {
  namber: '4234',
  age: 23
};

let newProp = prompt("Имя нового поля?", ""); //подразумевается что ответ будет "he  llo"
let newPropContent = prompt("Что будет в новом поле?");
// у объекта car создалось новое свойство с именем из значение как ответил пользователь
car[newProp] = newPropContent;
console.log(car["he  llo"]);
// в этом случае когда мы спрашиваем у пользователя имя нового свойства и сохраняем его как свойство в объек,
// это имя сораняестя как идекс элемента ассоциативного массива


//создание метода вне объекта
person.newMethod = function () {
  console.log("bi-bi");
}

//
console.log(person)
console.log(person.name)
console.log(person['year'])
console.log(person.car.year)

person.year = 1999
console.log(person)

person.calculateAge()
console.log(person)

person.hugeNAge(10)
console.log(person)

//Определение количества свойст у объекта
Object.keys(myObj).length
