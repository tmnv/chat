const fs = require("fs");
const path = require("path");
const WebSocket = require("ws");
const https = require("https");

const PROJECT_DIR = "C:/Users/Batman/Desktop/web/JS/Projects/technocom/";
const TLS_DIR = PROJECT_DIR + "/tls";
//const ROOT_DIR = PROJECT_DIR + "/chat/lab/puzankov/client";
const options = {
  key: fs.readFileSync(path.join(TLS_DIR, "chat_private.key")),
  cert: fs.readFileSync(path.join(TLS_DIR, "chat_public.crt")),
};

const port = 3000;
const httpsServer = https.createServer(options, (req, res) => {
  console.log(req.url);
  res.writeHead(200, {
    "Content-Type": "text/html; charset=utf-8",
  });
  res.end("<h1>WSS</h1>");
});
httpsServer.listen(port);

const wssServer = new WebSocket.Server({
  server: httpsServer,
  // "port": port
});

console.log("Сервер WSS слушает порт " + port);

wssServer.on("connection", (ws) => {
  let message = {
    name: "Сервер",
    text: "Добро пожаловать в чат сообщества Техноком.",
  };
  ws.send(addDateToMessage(message));

  ws.on("message", (message) => {
    wssServer.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        messageJson = JSON.parse(message);
        client.send(addDateToMessage(messageJson));
      }
    });
  });
});

const addDateToMessage = (message) => {
  message.date = new Date();

  return JSON.stringify(message);
};

//const port = 3000;
// const server = new WebSocket.Server({"port": port})

// console.log('Сервер WS слушает порт ' + port)

// server.on('connection', ws => {
//     let message = {
//         'name': 'Сервер',
//         'text': 'Добро пожаловать в чат сообщества Техноком.'
//     }
//     ws.send(addDateToMessage(message))

//     ws.on('message', message => {
//         server.clients.forEach(client => {
//             if (client.readyState === WebSocket.OPEN) {
//                 messageJson = JSON.parse(message)
//                 client.send(addDateToMessage(messageJson))

//             }
//         })
//     })
// })

// const addDateToMessage = message => {
//     message.date = new Date();

//     return JSON.stringify(message);
// }
