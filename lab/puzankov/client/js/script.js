// Элемент содержащий статус подключения к WebSocket.
let statusEl, settingsBtnEl, settingsEl;
// Тут пользователь указывает своё имя
let usernameEl;
// В этом элементе пользователь вводит текст сообщения.
let messageEl;
// В этот элемент попадают новые сообщения.
let messageListEl;
//задаём переменную для аудио элемента
let incomingMessageSound;
//для хранения схемы //для хранения хоста //для хранения порта
let settingsSchemaEl, settingHostEl, settingPortEl;
//для хранение видео-элемента
let videoBtn; //, innerVideoWindow,

let ws;

//кнопка сохранения в local storage
let settingSaveBtn;
// Настройки по умолчанию
let settings = {
  schema: "ws",
  host: "localhost",
  port: "3000",
};

document.addEventListener("DOMContentLoaded", (event) => {
  // console.log('Страница загружена')
  //console.log(event)
  let btn = document.getElementById("send");
  btn.addEventListener("click", handleSendClick);

  // Получаем элемент содержащий статус подключения к WebSocket.
  statusEl = document.getElementById("status");
  settingsBtnEl = document.getElementById("settingsBtn");
  settingsBtnEl.addEventListener("click", handleSetBtnClick);
  //получаем элемент, куда будет введено имя пользователя
  usernameEl = document.getElementById("username");
  // В этом элементе пользователь вводит текст сообщения.
  messageEl = document.getElementById("messageText");
  messageEl.addEventListener("keydown", handleKeydown);
  // В этот элемент попадают новые сообщения.
  messageListEl = document.getElementById("messageList");
  //добавили аудио-блок в переменную
  incomingMessageSound = document.getElementById("incomingMessage");
  //добавили видео-блок в переменную
  //innerVideoWindow = document.querySelector("#video");
  videoBtn = document.querySelector("#videoButton");
  videoBtn.addEventListener("click", handleVideoClick);

  let settingString = localStorage.getItem("settings");
  // console.log('settingsString', settingsString)
  if (settingString !== null) {
    settings = JSON.parse(settingString);

    if (settings) {
      ws = new WebSocket(
        `${settings.schema}://${settings.host}:${settings.port}`
      );
      ws.onopen = () => setStatus("Сервер CONNECTED");
      ws.onclose = () => setStatus("Сервер DISCONNECTED");
      ws.onmessage = (response) => {
        //console.log(response);
        addMessageToList(response.data);
        incomingMessageSound.play();
      };
    }
  }

  // функция, которая проверяет, есть ли local storage имя
  let savedNameString = localStorage.getItem("savedName");
  if (savedNameString !== null) {
    savedName = savedNameString;
    if (savedName) {
      usernameEl.value = savedName;
    }
  }

  settingsSchemaEl = document.getElementById("schema");
  settingsSchemaEl.value = settings.schema;

  settingsHostEl = document.getElementById("host");
  settingsHostEl.value = settings.host;

  settingsPortEl = document.getElementById("port");
  settingsPortEl.value = settings.port;

  settingsSaveBtn = document.getElementById("save");
  settingsSaveBtn.addEventListener("click", handleSaveClick);
});

const handleSetBtnClick = (event) => {
  event.preventDefault();
  settingsEl = document.getElementById("settings");
  //console.log(settingsEl)
  if (settingsEl.style.display === "none") {
    settingsEl.style.display = "block";
  } else {
    settingsEl.style.display = "none";
  }
};
//вешаем обработчик на событие клика по кнопке "Save", пакуем в строку и пуляем в local storage
const handleSaveClick = (event) => {
  event.preventDefault();

  settings = {
    schema: settingsSchemaEl.value,
    host: settingsHostEl.value,
    port: settingsPortEl.value,
  };

  localStorage.setItem("settings", JSON.stringify(settings));

  if (ws) {
    ws.close();
  }

  ws = new WebSocket(`${settings.schema}://${settings.host}:${settings.port}`);
  console.log("ws.readyState = " + ws.readyState);

  ws.onerror = (event) => {
    console.error("Ошибка подключения", event);
    settingsEl.style.backgroundColor = "red";
  };

  ws.onopen = () => {
    setStatus("Сервер ПОДКЛЮЧЕН");
    settingsEl.style.backgroundColor = "green";
    setTimeout(() => {
      settingsEl.style.display = "none";
    }, 3000);
  };
  ws.onclose = () => setStatus("Сервер ОТКЛЮЧЕН");

  ws.onmessage = (response) => {
    //console.log(response)
    addMessageToList(response.data);
    incomingMessageSound.play();
  };
};

const setStatus = (value) => {
  statusEl.innerHTML = value;
};

const handleSendClick = (event) => {
  event.preventDefault();
  sendMessageToServer(messageEl.value);
};

const handleKeydown = (event) => {
  //console.log(event);
  if (event.key === "Enter") {
    // Если не отменить перенос, то после(!) отправки сообщения и очистки формы перенос будет
    // добавлен автоматически.
    event.preventDefault();
    sendMessageToServer(messageEl.value);
    localStorage.setItem("savedName", usernameEl.value);
  }
};

const sendMessageToServer = (messageText) => {
  username = usernameEl.value.trim();

  if (username === "") {
    alert("Укажите своё имя.");
    usernameEl.focus();
    return;
  }

  try {
    if (messageText.trim() === "") {
      alert("Напишите сообщение.");
      messageEl.focus();
      return;
    }

    let message = {
      name: username,
      text: messageText,
    };

    // Пакуем сообщение в строку и отправляем его на WS сервер.
    let packedJson = JSON.stringify(message);
    ws.send(packedJson);

    // Стираем старое сообщение в текстовом элементе и возвращаем ему фокус (кнопка при нажатии получает фокус).
    messageEl.value = "";
    messageEl.focus();

    //закидываем в local storage введное в поле имя
    localStorage.setItem("savedName", usernameEl.value);
  } catch (e) {
    console.log(e);
  }
};

const formatDate = (date) => {
  return (
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1).toString().padStart(2, "0") +
    "-" +
    date.getDate() +
    " " +
    date.getHours().toString().padStart(2, "0") +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds()
  );
};

const addMessageToList = (messageText) => {
  let message = JSON.parse(messageText);

  // Создаём элемент для нового сообщения и помещаем в него написанную строку текста.
  let newMessageEl = document.createElement("div");
  newMessageEl.classList.add("message");

  let dateEl = document.createElement("div");
  dateEl.classList.add("message__date");
  dateEl.innerHTML = formatDate(new Date(message.date));
  newMessageEl.appendChild(dateEl);

  let nameEl = document.createElement("div");
  nameEl.classList.add("message__name");
  nameEl.innerHTML = message.name;
  newMessageEl.appendChild(nameEl);

  let textEl = document.createElement("div");
  textEl.classList.add("message__text");
  textEl.innerHTML = message.text;
  newMessageEl.appendChild(textEl);

  // Вставляем новое сообщение в список
  messageListEl.appendChild(newMessageEl);
};

const handleVideoClick = (event) => {
  navigator.mediaDevices
    .getUserMedia({
      audio: false,
      video: true,
    })
    .then((stream) => {
      innerVideo.srcObject = stream;
    })
    .catch(console.error("ERROR - Ошибка подключения userMedia"));
};
