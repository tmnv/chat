const https = require("https");
const http = require("http");
const fs = require("fs");
const path = require("path");

const PROJECT_DIR = "C:/Users/Batman/Desktop/web/JS/Projects/technocom/";
const ROOT_DIR = PROJECT_DIR + "chat/lab/puzankov/client/";
const TLS_DIR = PROJECT_DIR + "/tls";

const HOST = "localhost";
const HTTP_PORT = 8080;
const HTTPS_PORT = 8443;

const options = {
  key: fs.readFileSync(path.join(TLS_DIR, "chat_private.key")),
  cert: fs.readFileSync(path.join(TLS_DIR, "chat_public.crt")),
};

const server = https.createServer(options, (req, res) => {
  console.log(req.url);
  const writeHead = () => {
    let fileExtensionWithDot = path.extname(requestedResource);

    let contentType = "";

    if (fileExtensionWithDot === "") {
      console.log(
        `У запрашиваемого ресурса ${requestedResource} нет расширения`
      );
      return;
    }

    let fileExtension = fileExtensionWithDot.slice(1);

    switch (fileExtension) {
      case "html":
      case "css":
        contentType = `text/${fileExtension}; charset=utf-8`;
        break;
      case "js":
        contentType = "application/javascript; charset=utf-8";
        break;
      case "jpeg":
      case "jpg":
      case "png":
        fileExtension = fileExtension === "jpg" ? "jpeg" : fileExtension;
        contentType = "image/" + fileExtension;
        break;
      case "ico":
        contentType = "image/x-icon";
        break;
      case "mp3":
        contentType = "audio/mp3";
        break;
      default:
        console.log(
          `Запрошен ресурс ${requestedResource} c неизвестным расширением ${fileExtension}`
        );
    }

    res.writeHead(200, {
      "Content-type": contentType, //writeHead сама себя вызывает?
    });
  };

  const sendResponse = (err, content) => {
    if (err) {
      console.log(err);
      res.writeHead(404, {
        "Content-Type": "text/html; charset=utf-8",
      });
      res.end("<h1> Файл не найден </h1>");
      return;
    }
    writeHead();
    res.end(content);
  };

  let requestedResource = "";

  switch (req.url) {
    case "/":
      requestedResource = "/page/plan.html";
      break;
    case "/chat":
    case "/participants":
      requestedResource = `/page${req.url}.html`;
      break;
    default:
      requestedResource = req.url;
  }

  requestedResource = path.normalize(requestedResource);

  fs.readFile(ROOT_DIR + requestedResource, sendResponse);
});

const httpServer = http.createServer((req, res) => {
  let location = `https://${HOST}:${HTTPS_PORT}${req.url}`; // вариант для http соединения?

  res.writeHead(301, {
    Location: location,
  });
  res.end();
});

server.listen(HTTPS_PORT, () => {
  console.log(`HTTPS-сервер запущен и слушает порт ${HTTPS_PORT}`);
});

httpServer.listen(HTTP_PORT, () => {
  console.log(`HTTP-сервер запущен и слушает порт ${HTTP_PORT}`);
});

// const server = http.createServer((req, res) => {
//     //console.log(req.url)
//     if (
//         req.url === '/'
//         || req.url === 'chat.html'
//     ) {
//         res.writeHead(200, {
//             "Content-Type": "text/html; charset=utf-8"
//         })

//         fs.readFile(path.join(
//             __dirname, '/page/chat.html'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }

//     else if (req.url === '/page/plan') {
//         console.log('path to Plan' + req.url)
//         res.writeHead(200, {
//             "Content-Type": "text/html; charset=utf-8"
//         })
//         fs.readFile(path.join(
//             __dirname, '/page/plan.html'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }

//     else if (req.url === '/page/participants') {
//         res.writeHead(200, {
//             "Content-Type": "text/html; charset=utf-8"
//         })
//         fs.readFile(path.join(
//             __dirname, '/page/participants.html'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }

//     else if (req.url === '/js/script.js') {
//         res.writeHead(200, {
//             "Content-Type": "application/javascript; charset=utf-8"
//         })
//         fs.readFile(path.join(
//             __dirname, '/js/script.js'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }

//     else if (req.url === '/js/templates.js') {
//         res.writeHead(200, {
//             "Content-Type": "application/javascript; charset=utf-8"
//         })
//         fs.readFile(path.join(
//             __dirname, '/js/templates.js'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }

//     else if (req.url === '/css/style.css') {
//         res.writeHead(200, {
//             "Content-Type": "text/css; charset=utf-8"
//         })
//         fs.readFile(path.join(
//             __dirname, '/css/style.css'),
//             'utf-8',
//             (err, content) => {
//                 if (err) {
//                     // Пока ничего не делаем.
//                 }

//                 res.end(content)
//             }
//         )
//     }
//     else if (req.url === '/img/lenin-i-kommunizm.jpg') {
//         res.writeHead(200, {
//             "Content-Type": "image/jpeg"
//         })
//         fs.readFile((ROOT_DIR + '/img/lenin-i-kommunizm.jpg'), (err, content) => {
//             if (err) {
//                 console.log(err);
//             }

//             res.end(content)
//         })
//     }
//     else if (req.url === '/favicon.ico') {
//         res.writeHead(200, {
//             "Content-Type": "image/x-icon"
//         })
//         fs.readFile((ROOT_DIR + 'favicon.ico'), (err, content) => {
//             if (err) {
//                 console.log(err);
//             }

//             res.end(content)
//         })
//     }
//     //разобраться
//     else if (req.url === '/audio/incomingMessage.mp3') {
//         res.writeHead(200, {
//             "Content-Type": "audio/mp3"
//         })
//         fs.readFile((ROOT_DIR + '/audio/incomingMessage.mp3'), (err, content) => {
//             if (err) {
//                 console.log(err);
//             }
//             try {
//             res.end(content);
//           }
//           catch(e) {
//               console.log(e)
//           }
//         })

//     }

//     else if (req.url === '/img/frontend-map.png') {
//         res.writeHead(200, {
//             "Content-Type": "image/png"
//         })
//         fs.readFile((ROOT_DIR + '/img/frontend-map.png'), (err, content) => {
//             if (err) {
//                 throw err
//             }

//             res.end(content)
//         })
//     }
//     else {
//         //console.log('Запрошен ресурс неизвестный ресурс ' + req.url + '.')
//     }
// })

// let port = 8080
// server.listen(port, () => {
//     console.log('Сервер запущен и слушает порт ' + port + '.')
// })
