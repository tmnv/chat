// Подробности:
// https://www.youtube.com/watch?v=DKOaL94VyFY

const WebSocket = require("ws");

const port = 3000;
const server = new WebSocket.Server({ port: port });

console.log("Сервер WS слушает порт " + port);

server.on("connection", (ws) => {
  ws.send("Добро пожаловать в чат сообщества Техноком.");

  ws.on("message", (message) => {
    server.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });
});
