const http = require('http')
const fs = require('fs')
const path = require('path')

//let ROOT_DIR = '/var/www/su/tehnokom/chat/client/'
let ROOT_DIR = 'C:/Users/User/chat/lab/zinahim/client/'

//

const PROJECT_DIR = 'C:/Users/User/chat/lab/zinahim/'
// let ROOT_DIR = PROJECT_DIR + '/client'
//const ROOT_DIR = PROJECT_DIR + 'chat/lab/asidorov/client/'
const TLS_DIR = PROJECT_DIR + '/tls'

// const options = {
//     "key": fs.readFileSync(path.join(TLS_DIR, 'chat_private.key')),
//     "cert": fs.readFileSync(path.join(TLS_DIR, 'chat_public.crt'))
// }

const server = http.createServer((req, res) => {
    const writeHead = () => {
        let fileExtensionWithDot = path.extname(requestedResource)

        let contentType = ''

        if (fileExtensionWithDot === '') {
            console.log(`У запрошенного ресурса ${requestedResource} нет расширения.`)
            return
        }

        let fileExtension = fileExtensionWithDot.slice(1)

        switch(fileExtension) {
            case "html":
            case "css":
                contentType = `text/${fileExtension}; charset=utf-8`
                break
            case "js":
                contentType = "application/javascript; charset=utf-8"
                break
            case "jpeg":
            case "jpg":
            case "png" :
                fileExtension = (fileExtension === 'jpg') ? "jpeg" : fileExtension
                contentType = "image/" + fileExtension
                break
            case "ico":
                contentType = "image/x-icon"
                break
            case "mp3":
                contentType = "audio/mp3"
                break
            default:
                console.log(`Запрошен ресурс ${requestedResource} с неизвестным расширением ${fileExtension}`)
        }

        res.writeHead(200, {
            "Content-Type": contentType
        })
    }

    const sendResponse = (err, content) => {
        if (err) {
            console.log(err)
            res.writeHead(404, {
                "Content-Type": "text/html; charset=utf-8"
            })
            res.end('<h1>Файл не найден</h1>')
            return
        }

        writeHead()

        res.end(content)
    }

    let requestedResource = ''

    switch (req.url) {
        case '/':
            requestedResource = '/page/index.html'
            break
        case '/plan':
        case '/list':
            requestedResource = `/page${req.url}.html`
            break
        default:
            requestedResource = req.url
    }

    requestedResource = path.normalize(requestedResource)

    fs.readFile(
        (ROOT_DIR + requestedResource),
        sendResponse
    )
})

let port = 1088
server.listen(port, () => {
    console.log('Сервер запущен и слушает порт ' + port + '.')
})
