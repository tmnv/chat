/*
Класс для хранения в файлах данных в формате json.
 */
const fs = require('fs')
const path = require('path')

class StorageFs {
    // name - имя файла в папке data
    // static name = 'issues.json'      // На сервере это объявление вызвало ошибку

    static getAll() {
        if (StorageFs.name === '') {
            console.log('Не указан файл хранилища')
            return []
        }

        let fileContent = fs.readFileSync(
            // path.join(__dirname, '..', 'data', StorageFs.name),
            path.join(__dirname, '..', 'data', 'issues.json'),
            'utf-8'
        )

        try {
            return JSON.parse(fileContent)
        }
        catch (ex) {
            console.log(ex)
            return []
        }
    }

    static getById(id) {
        let objects = StorageFs.getAll()
        let obj = {}

        for (let i = 0; i < objects.length; i++) {
            let item = objects[i]
            if (
                item.id
                && item.id === id
            ) {
                obj = item
                break
            }
        }

        return obj
    }

    static update(obj) {
        let objects = StorageFs.getAll()

        for (let i = 0; i < objects.length; i++) {
            let item = objects[i]
            if (
                item.id
                && item.id === obj.id
            ) {
                objects[i] = obj
                break
            }
        }

        fs.writeFileSync(
            // path.join(__dirname, '..', 'data', StorageFs.name),
            path.join(__dirname, '..', 'data', 'issues.json'),
            JSON.stringify(objects),
            'utf8'
        )
    }

    static insert(obj) {
        let objects = StorageFs.getAll()
        objects.push(obj)

        fs.writeFileSync(
            // path.join(__dirname, '..', 'data', StorageFs.name),
            path.join(__dirname, '..', 'data', 'issues.json'),
            JSON.stringify(objects),
            'utf8'
        )
    }

    static delete(id) {
        let objects = StorageFs.getAll()

        for (let i = 0; i < objects.length; i++) {
            let item = objects[i]
            if (
                item.id
                && item.id === id
            ) {
                objects.splice(i, 1)
                break
            }
        }

        fs.writeFileSync(
            // path.join(__dirname, '..', 'data', StorageFs.name),
            path.join(__dirname, '..', 'data', 'issues.json'),
            JSON.stringify(objects),
            'utf8'
        )
    }
}

module.exports = StorageFs
