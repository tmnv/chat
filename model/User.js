/*
Пользователь представляет из себя учётные данные, которые используются конкретным человеком (Person)
для получения доступа к возможностям сайта.
 */

const {v4: uuidv4} = require('uuid');

class User {
    constructor(obj) {
        this.id = obj.id || '';
        this.email = obj.email || '';
        this.person = null;
        this.salt = obj.salt || '';
        this.passwordHash = obj.passwordHash || '';
    }
}

module.exports = User
