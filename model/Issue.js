/*
Класс описывающий выявленные проблемы.
 */
const {v4: uuidv4} = require('uuid')
const Common = require('./Common')

class Issue {
    constructor(obj) {
        this.id = obj.id || '';
        this.dateOfCreation = obj.dateOfCreation ? new Date(obj.dateOfCreation) : new Date();
        this.dateOfChange = obj.dateOfChange ? new Date(obj.dateOfChange) : this.dateOfCreation;
        // console.log('dateOfCreation', this.dateOfCreation);
        // console.log('dateOfChange', this.dateOfChange);
        this.title = obj.title ? Common.sanitizeText(obj.title) : '';
        this.closed = obj.closed || false;
    }

    generateId() {
        this.id = uuidv4();
    }

    updateDateOfChange() {
        this.dateOfChange = new Date();
    }

    isClosed() {
        return this.closed;
    }

    open() {
        this.closed = false;
    }

    close() {
        this.closed = true;
    }

    getFormattedDateOfCreation() {
        return Common.formatDate(this.dateOfCreation);
    }

    getFormattedDateOfChange() {
        return Common.formatDate(this.dateOfChange);
    }

    toJSON() {
        return {
            "id": this.id,
            "dateOfCreation": this.dateOfCreation,
            "dateOfChange": this.dateOfChange,
            "title": this.title,
            "closed": this.closed
        };
    }
}

module.exports = Issue
