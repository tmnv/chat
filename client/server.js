const http = require('http');
const https = require('https');
const querystring = require('querystring');
const fs = require('fs');
const path = require('path');
const {v4: uuidv4} = require('uuid');
const crypto = require('crypto');

const PROJECT_DIR = '/var/www/su/tehnokom';
const ROOT_DIR = PROJECT_DIR + '/chat/client';
const TLS_DIR = PROJECT_DIR + '/tls';
const DATA_DIR = `${PROJECT_DIR}/chat/data`;
const MODEL_DIR = `${PROJECT_DIR}/chat/model`;

const Issue = require(`${MODEL_DIR}/Issue`);
const StorageFs = require(`${MODEL_DIR}/StorageFs`);

const DEBUG_MODE = false;
const HOST = DEBUG_MODE ? 'localhost' : 'learn.tehnokom.su';
const HTTP_PORT = DEBUG_MODE ? 8080 : 80;
const HTTPS_PORT = DEBUG_MODE ? 8443 : 443;

const options = {
    "key": fs.readFileSync(path.join(TLS_DIR, `${HOST}_private.key`)),
    "cert": fs.readFileSync(path.join(TLS_DIR, `${HOST}_public.crt`))
}

// let testArr = [];

const server = https.createServer(options, (req, res) => {
    // Описание запроса req (класс http.ClientRequest)
    // https://nodejs.org/dist/latest-v12.x/docs/api/http.html#http_class_http_clientrequest

    // Описание ответа res (класс http.ServerResponse)
    // https://nodejs.org/dist/latest-v12.x/docs/api/http.html#http_class_http_serverresponse

    // Данный тестовый массив сохраняется между вызовами, то есть постоянно находится в памяти.
    // testArr.push(uuidv4());
    // console.log(testArr);
    // Видео про работу с сессиями
    // https://www.youtube.com/watch?v=T_wKXuWW4Wo
    const {method, url, headers} = req;
    // console.log(`${method} ${url}`, headers);

    let cookies = {}
    if (headers.cookie) {
        // console.log('headers.cookie', headers.cookie);

        const items = (headers.cookie).split(';');
        // console.log('items', items);

        for (let i = 0; i < items.length; i++) {
            let item = items[i];
            // console.log('item', item);

            const parts = item.split('=');
            // console.log('parts', parts);

            const key = parts[0].trim();
            const val = parts[1] || '';
            cookies[key] = val.trim();
        }
    }

    console.log('cookies', cookies);
    console.log(cookies.SID);

    let SID;
    // Работающая установка cookie
    if (cookies.SID) {
        SID = cookies.SID;
    }
    else {
        SID = uuidv4();
    }
    // SID=31d4d96e407aad42; - пара ключ-значение, которое должен хранить браузер.
    // Expires=Wed, 09 Jun 2021 10:18:14 GMT; - точная дата истечения действия куки.
    // Max-Age=3600 - количество секунд до истечения куки после его получения (важнее Expires).
    // Domain=tehnokom.su; - теперь все сайты с доменом tehnokom.su и его поддоменами будут получать этот куки.
    //   Если не указать Domain, то браузер должен указать тот хост, с которого был получен данный куки.
    // Path=/; - путь к ресурсу, начиная с которого браузер будет возвращать куки. / означает для любой страницы.
    // Secure; - передавать куки только по защищённому каналу.
    // HttpOnly; - запрещает скриптам на стороне клиента работать с данным файлом куки.
    //   Это нужно чтобы чужие скрипты (от Яндекс Карт, например) не могли посмотреть идентификатор сессии.
    res.setHeader('Set-Cookie', `SID=${SID}; Domain=${HOST}; Path=/; Secure; HttpOnly;`);
    // res.setHeader('Set-Cookie', `mycookie=test; Max-Age=3600; Domain=${HOST}; Path=/;`);

    const writeHead = () => {
        let fileExtensionWithDot = path.extname(requestedResource);

        let contentType = '';

        if (fileExtensionWithDot === '') {
            console.log(`У запрошенного ресурса ${requestedResource} нет расширения.`)
            return
        }

        let fileExtension = fileExtensionWithDot.slice(1);

        switch (fileExtension) {
            case "html":
            case "css":
                contentType = `text/${fileExtension}; charset=utf-8`;
                break
            case "js":
                contentType = "application/javascript; charset=utf-8";
                break
            case "jpeg":
            case "jpg":
            case "png":
                fileExtension = (fileExtension === 'jpg') ? "jpeg" : fileExtension;
                contentType = "image/" + fileExtension;
                break
            case "ico":
                contentType = "image/x-icon";
                break
            case "mp3":
                contentType = "audio/mp3";
                break
            default:
                console.log(`Запрошен ресурс ${requestedResource} с неизвестным расширением ${fileExtension}`)
        }

        res.writeHead(200, {
            "Content-Type": contentType
        })
    }

    const replacePlaceholdersInTemplate = (replacements, template) => {
        for (let placeholder in replacements) {
            template = template.replace(`{{${placeholder}}}`, replacements[placeholder]);
        }

        return template;
    }

    const sendResponse = (err, content) => {
        if (err) {
            console.log(err);
            res.writeHead(404, {
                "Content-Type": "text/html; charset=utf-8"
            });
            return;
        }

        writeHead();

        res.end(content)
    }

    // Получаем путь и параметры. Тут требуется указать базу для разбора URL.
    // https://learn.tehnokom.su/issues?delete=660bcfc0-f074-4bc1-bc48-658e296163bd
    const {pathname, searchParams} = new URL(req.url, 'https://learn.tehnokom.su');

    // Анатомия строки URL
    // https://learn.tehnokom.su/issues?del=qwe123&test&var=asd#link
    //
    // https: - протокол
    // learn.tehnokom.su - имя хоста
    // /issues - путь к ресурсу
    // ? - отделяет путь от параметров
    // del=qwe123&test&var=asd - параметры
    // #link - ссылка на ID конкретного элемента на странице

    let requestedResource = '';

    switch (pathname) {
        case '/':
            requestedResource = '/page/plan.html';
            break
        case '/chat':
        case '/list':
        case '/soul':
        case '/theory':
        case '/manifest':
        case '/auth/login':
            if (method === 'POST') {
                let body = '';
                req.on('data', chunk => {
                    body += chunk.toString();
                })
                req.on('end', () => {
                    body = querystring.parse(body);

                    console.log(body);

                    // let issue = new Issue(body);
                    // issue.generateId();
                    // StorageFs.insert(issue.toJSON());

                    res.writeHead(301, {
                        Location: "/auth/login"
                    });
                    res.end();
                })

                return
            }
            requestedResource = `/page${pathname}.html`;
            break;
        case '/auth/registration':
            if (method === 'POST') {
                let body = '';
                req.on('data', chunk => {
                    body += chunk.toString();
                })
                req.on('end', () => {
                    body = querystring.parse(body);

                    console.log(body);

                    // let issue = new Issue(body);
                    // issue.generateId();
                    // StorageFs.insert(issue.toJSON());

                    res.writeHead(301, {
                        Location: "/auth/login"
                    });
                    res.end();
                })

                return
            }
            requestedResource = `/page${pathname}.html`;
            break;
        case "/issues":
            if (req.method === 'POST') {
                let body = ''
                req.on('data', chunk => {
                    body += chunk.toString();
                })
                req.on('end', () => {
                    body = querystring.parse(body);

                    let issue = new Issue(body);
                    issue.generateId();
                    StorageFs.insert(issue.toJSON());

                    res.writeHead(301, {
                        Location: "/issues"
                    });
                    res.end();
                })

                return
            }

            let error = '';
            let issueId = searchParams.get('id');
            if (issueId) {
                let action = searchParams.get('action');
                switch (action) {
                    case 'open':
                    case 'close':
                        let issue = new Issue(StorageFs.getById(issueId));
                        if (issue.id !== '') {
                            // issue.closed = ! issue.closed;
                            issue.isClosed() ? issue.open() : issue.close();
                            issue.updateDateOfChange();

                            StorageFs.update(issue.toJSON());
                        }

                        res.writeHead(301, {
                            Location: "/issues"
                        });
                        res.end();

                        return;
                    case 'delete':
                        StorageFs.delete(issueId)

                        res.writeHead(301, {
                            Location: "/issues"
                        });
                        res.end()

                        return;
                    default:
                        error = 'Запрошено неизвестное действие с вопросом.'
                }
            }

            let content = '<h2>Вопросов нет</h2>'
            let issues = StorageFs.getAll();
            if (issues.length > 0) {
                content = '<table>';
                for (let i = 0; i < issues.length; i++) {
                    let issue = new Issue(issues[i]);
                    let number = i + 1;
                    let title = issue.isClosed() ? `<s>${issue.title}</s>` : issue.title;
                    let action = issue.isClosed() ? 'open' : 'close';
                    let actionName = issue.isClosed() ? 'Открыть' : 'Закрыть';
                    content += `
    <tr>
        <td>${number}</td>
        <td><span style="font-size: 0.7em;">Создано: <b style="color: blue;">${issue.getFormattedDateOfCreation()}</b>. Изменено: <b style="color: red;">${issue.getFormattedDateOfChange()}</b></span><br>${title}</td>
        <td><a href="/issues?id=${issue.id}&action=${action}">${actionName}</a></td>
        <td><a href="/issues?id=${issue.id}&action=delete">Удалить</a></td>
    </tr>`;
                }
                content += '</table>'
            }

            requestedResource = `/page${pathname}/list.html`

            let template = fs.readFileSync(
                path.normalize(ROOT_DIR + requestedResource),
                'utf8'
            )

            sendResponse('', replacePlaceholdersInTemplate({"table": content}, template));

            return
        case "/issues/add":
            requestedResource = `/page${pathname}.html`
            break
        default:
            requestedResource = pathname
    }

    requestedResource = path.normalize(requestedResource)

    fs.readFile((ROOT_DIR + requestedResource), sendResponse)
});

const httpServer = http.createServer((req, res) => {
    let location = `https://${HOST}:${HTTPS_PORT}${req.url}`

    res.writeHead(301, {
        'Location': location
    });
    res.end();
})

server.listen(HTTPS_PORT, () => {
    console.log(`HTTPS-сервер запущен и слушает порт ${HTTPS_PORT}.`)
})

httpServer.listen(HTTP_PORT, () => {
    console.log(`HTTP-сервер запущен и слушает порт ${HTTP_PORT}.`)
})
