document.querySelector('header').innerHTML = `
<nav style="display: flex; align-items: center;">
    <div style="flex-grow: 1;">
    <a href="/">План развития</a>
    <a href="/manifest">Манифест КП</a>
    <a href="/chat">Чат</a>
    <a href="/list">Список участников</a>
    <a href="/soul">Психология</a>
    <a href="/issues">Вопросы</a>
    <a href="/theory">Теория</a>
    </div>
    <div>
<!--    <button onclick="showModal('block')" style="display: flex; align-items: center;"><img src="/img/login.png"/><span>Войти</span></button>-->
    <a href="/auth/login" class="button" style="margin: 0; display: flex; align-items: center;"><img src="/img/login.png" style="background-color: #ffffff;"/><span>Войти</span></a>
    </div>
</nav>`;
